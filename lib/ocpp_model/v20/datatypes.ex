defmodule OcppModel.V20.DataTypes do
  @moduledoc """
  Field types or subclasses of the OCPP Model
  """

  defmodule ACChargingParameters do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:energyAmount, integer(), enforce: true)
      field(:evMinCurrent, integer(), enforce: true)
      field(:evMaxCurrent, integer(), enforce: true)
      field(:evMaxVoltage, integer(), enforce: true)
    end
  end

  defmodule AdditionalInfo do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..36 identifierString
      field(:additionalIdToken, String.t(), enforce: true)
      # 0..50
      field(:type, String.t(), enforce: true)
    end
  end

  defmodule AuthorizationData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:idTokenInfo, IdTokenInfo.t())
      field(:idToken, IdToken.t(), enforce: true)
    end
  end

  defmodule CertificateHashData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # HashAlgorithmEnumType
      field(:hashAlgorithm, String.t(), enforce: true)
      field(:issuerNameHash, String.t(), enforce: true)
      field(:issureKeyHash, String.t(), enforce: true)
      field(:serialNumber, String.t(), enforce: true)
    end
  end

  defmodule CertificateHashDataChain do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetCertificateIdUse
      field(:certificateType, String.t(), enforce: true)
      field(:certificateHashData, CertificateHashData.t(), enforce: true)
      # 0..4
      field(:childCertificateHashData, list(CertificateHashData.t()))
    end
  end

  defmodule ChargingLimit do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ChargingLimitSource
      field(:chargingLimitSource, String.t(), enforce: true)
      field(:isGridCritical, boolean())
    end
  end

  defmodule ChargingNeeds do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # EnergyTransferModeEnumType
      field(:requestedEnergyTransfer, String.t(), enforce: true)
      field(:departureTime, String.t())
      field(:acChargingParameters, ACChargingParameters.t())
      field(:dcChargingParameters, DCChargingParameters.t())
    end
  end

  defmodule ChargingProfile do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:stackLevel, integer(), enforce: true)
      # ChargingProfilePurposeEnumType
      field(:chargingProfilePurpose, String.t(), enforce: true)
      # ChargingProfileKindEnumType
      field(:chargingProfileKind, String.t(), enforce: true)
      # RecurrencyKindEnumType
      field(:recurrencyKind, String.t())
    end
  end

  defmodule ChargingProfileCriterion do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ChargingProfilePurposeEnumType
      field(:chargingProfilePurpose, String.t())
      field(:stackLevel, integer())
      field(:chargingProfileId, list(integer()))
      # ChargingLimitSourceEnumType
      field(:chargingLimitSource, list(String.t()))
    end
  end

  defmodule ChargingSchedule do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:startSchedule, String.t())
      field(:duration, integer())
      # ChargingRateUnitEnumType
      field(:chargingRateUnit, String.t(), enforce: true)
      field(:minChargingRate, float())
      field(:chargingSchedulePeriod, list(ChargingSchedulePeriod.t()), enforce: true)
      field(:salesTariff, SalesTariff.t())
    end
  end

  defmodule ChargingSchedulePeriod do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:startPeriod, integer(), enforce: true)
      field(:limit, float(), enforce: true)
      # 1..3
      field(:numberPhases, integer())
      # 1..3
      field(:phaseToUse, integer())
    end
  end

  defmodule ChargingStation do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..25
      field(:serialNumber, String.t())
      # 0..20
      field(:model, String.t(), enforce: true)
      # 0..50
      field(:vendorName, String.t(), enforce: true)
      # 0..50
      field(:firmwareVersion, String.t())
      field(:modem, ModemType.t())
    end
  end

  defmodule ClearChargingProfile do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer())
      # ChargingProfilePurposEnumType
      field(:chargingProfilePurpose, String.t())
      field(:stackLevel, integer())
    end
  end

  defmodule ClearMonitoringResult do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ClearMonitoringStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:id, integer(), enforce: true)
      field(:statusInfo, StatusInfoType.t())
    end
  end

  defmodule Component do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:name, String.t(), enforce: true)
      field(:instance, String.t())
      field(:evse, Evse.t())
    end
  end

  defmodule ComponentVariable do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:component, Component.t(), enforce: true)
      field(:variable, Variable.t())
    end
  end

  defmodule CompositeSchedule do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer(), enforce: true)
      field(:duration, integer(), enforce: true)
      field(:scheduleStart, String.t(), enforce: true)
      # ChargingRateUnit EnumType
      field(:chargingRateUnit, String.t(), enforce: true)
      field(:chargingSchedulePeriod, list(ChargingSchedulePeriod.t()))
    end
  end

  defmodule ConsumptionCost do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:startValue, float(), enforce: true)
      field(:cost, Cost.t(), enforce: true)
    end
  end

  defmodule Cost do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # CostKindEnumType
      field(:costKind, String.t(), enforce: true)
      field(:amount, integer(), enforce: true)
      # -3 .. 3
      field(:amountMultiplier, integer())
    end
  end

  defmodule DCChargingParameters do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evMaxCurrent, integer(), enforce: true)
      field(:evMaxVoltage, integer(), enforce: true)
      field(:energyAmount, integer(), enforce: true)
      field(:evMaxPower, integer(), enforce: true)
      field(:stateOfCharge, integer(), enforce: true)
    end
  end

  defmodule EventData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:eventId, integer(), enforce: true)
      field(:timestamp, String.t(), enforce: true)
      # EventTriggerEnumType
      field(:trigger, String.t(), enforce: true)
      field(:cause, integer())
      field(:actualValue, String.t(), enforce: true)
      field(:techCode, String.t())
      field(:techInfo, String.t())
      field(:cleared, boolean())
      field(:transactionId, String.t())
      field(:variableMonitoringId, integer())
      # EventNotificationEnumType
      field(:eventNotificationType, String.t(), enforce: true)
      field(:component, Component.t(), enforce: true)
      field(:variable, Variable.t(), enforce: true)
    end
  end

  defmodule Evse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:connector_id, integer())
    end
  end

  defmodule IdToken do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:idToken, String.t(), enforce: true)
      # IdToken EnumType
      field(:type, String.t(), enforce: true)
      field(:additionalInfo, list(AdditionalInfoType.t()))
    end
  end

  defmodule IdTokenInfo do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # AuthorizationStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:cacheExpiryDateTime, String.t())
      field(:chargingPriority, integer())
      field(:language1, String.t())
      field(:evseId, list(integer()))
      field(:language2, String.t())
      field(:groupIdToken, IdTokenType.t())
      field(:personalMessage, String.t())
    end
  end

  defmodule GetVariableData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # AttributeEnumType
      field(:attributeType, String.t())
      field(:component, Component.t(), enforce: true)
      field(:variable, Variable.t(), enforce: true)
    end
  end

  defmodule GetVariableResult do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetVariableStatusEnumType
      field(:attributeStatus, String.t(), enforce: true)
      # AttributeEnumtype
      field(:attributeType, String.t())
      field(:attributeValue, String.t())
      field(:component, Component.t(), enforce: true)
    end
  end

  defmodule LogParameters do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:remoteLocation, String.t(), enforce: true)
      field(:oldestTimestamp, String.t())
      field(:latestTimestamp, String.t())
    end
  end

  defmodule MessageContent do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # MessageFormatEnumtype
      field(:format, String.t(), enforce: true)
      # 0..8
      field(:language, String.t())
      # 0..512
      field(:content, String.t())
    end
  end

  defmodule MessageInfo do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      # MessagePriorityEnumType
      field(:priority, String.t(), enfore: true)
      # MessageStateEnumType
      field(:state, String.t())
      field(:startDateTime, String.t())
      field(:endDateTime, String.t())
      field(:transactionId, String.t())
      field(:message, MessageContent.t())
    end
  end

  defmodule MeterValue do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # dateTime
      field(:timestamp, String.t(), enforce: true)
      field(:sampledValue, SampledValueType.t(), enforce: true)
    end
  end

  defmodule Modem do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..20
      field(:iccid, String.t())
      # 0..20
      field(:imsi, String.t())
    end
  end

  defmodule MonitoringData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:component, Component.t(), enforce: true)
      field(:variable, Variable.t(), enforce: true)
      field(:variableMonitoring, list(VariableMonitoring.t()), enforce: true)
    end
  end

  defmodule OCSPRequestData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # HashAlgorithmEnumType
      field(:hashAlgorithm, String.t(), enforce: true)
      # 0..512
      field(:issuerNameHash, String.t(), enforce: true)
      # 0..128
      field(:issuerKeyHash, String.t(), enforce: true)
      # 0..40
      field(:serialNumber, String.t(), enforce: true)
      # 0..512
      field(:responderURL, String.t(), enforce: true)
    end
  end

  defmodule RelativeTimeInterval do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:start, integer(), enforce: true)
      field(:duration, integer())
    end
  end

  defmodule ReportData do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:component, Component.t(), enforce: true)
    end
  end

  defmodule SalesTariff do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:salesTariffDescription, String.t())
      field(:numEPriceLevels, integer())
      field(:salesTariffEntry, SalesTariffEntry.t())
    end
  end

  defmodule SalesTariffEntry do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:ePriceLevel, integer())
      field(:relativeTimeInterval, RelativeTimeInterval.t(), enforce: true)
      field(:consumptionCost, ConsumptionCost.t())
    end
  end

  defmodule SampledValue do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:value, number(), enforce: true)
      # ReadingContextEnumType
      field(:context, String.t())
      # MeasurerandEnumType
      field(:measurand, String.t())
      # PhaseEnumType
      field(:phase, String.t())
      # LocationEnumType
      field(:location, String.t())
      field(:signedMeterValue, SignedMeterValueType.t())
      field(:unitOfMeasure, UnitOfMeasureType.t())
    end
  end

  defmodule SignedMeterValue do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..2500
      field(:signedMeterData, String.t(), enforce: true)
      # 0..50
      field(:signingMethod, String.t(), enforce: true)
      # 0..50
      field(:encodingMethod, String.t(), enforce: true)
      # 0..2500
      field(:publicKey, String.t(), enforce: true)
    end
  end

  defmodule StatusInfo do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..20
      field(:reasonCode, String.t(), enforce: true)
      # 0..512
      field(:additionalInfo, String.t())
    end
  end

  defmodule Transaction do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..36
      field(:transactionId, String.t(), enforce: true)
      # ChargingStateEnumType
      field(:chargingState, String.t())
      field(:timeSpentCharging, integer())
      # ReasonEnumType
      field(:stoppedReason, String.t())
      field(:remoteStartId, integer())
    end
  end

  defmodule UnitOfMeasure do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..20
      field(:unit, String.t())
      field(:multiplier, integer())
    end
  end

  defmodule Variable do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:name, String.t(), enforce: true)
    end
  end

  defmodule VariableMonitoring do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:transaction, boolean(), enforce: true)
      field(:value, float(), enforce: true)
      # MonitorEnumType
      field(:type, String.t(), enforce: true)
      field(:severity, integer(), enforce: true)
    end
  end
end
