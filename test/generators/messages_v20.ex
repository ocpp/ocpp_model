defmodule Generators.V20.Messages do
  @moduledoc false

  use ExUnitProperties

  alias OcppModel.V20.Messages, as: M

  import Generators.V20.DataTypes
  import Generators.V20.Generic

  def authorize_request_generator do
    gen all(
          gen_certificate <- string_generator(5500),
          gen_id_token <- id_token_generator(),
          gen_ocsp_req_data <- list_generator(ocsp_req_data_generator(), length: 4)
        ) do
      %M.AuthorizeRequest{
        certificate: gen_certificate,
        idToken: gen_id_token,
        iso15118CertificateHashData: gen_ocsp_req_data
      }
    end
  end

  def authorize_response_generator do
    gen all(
          gen_certificate_status <- enum_generator(:authorizeCertificateStatus),
          gen_id_token_info <- id_token_info_generator()
        ) do
      %M.AuthorizeResponse{
        certificateStatus: gen_certificate_status,
        idTokenInfo: gen_id_token_info
      }
    end
  end

  def bootnotification_request_generator do
    gen all(
          gen_reason <- enum_generator(:bootReason),
          gen_charge_station <- charging_station_generator()
        ) do
      %M.BootNotificationRequest{
        reason: gen_reason,
        chargingStation: gen_charge_station
      }
    end
  end

  def bootnotification_response_generator do
    gen all(
          gen_datetime <- date_time_generator(),
          gen_interval <- integer_generator(1..900),
          gen_status <- enum_generator(:registrationStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.BootNotificationResponse{
        currentTime: gen_datetime,
        interval: gen_interval,
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def cancel_reservation_request_generator do
    gen all(gen_reservation_id <- integer_generator()) do
      %M.CancelReservationRequest{
        reservationId: gen_reservation_id
      }
    end
  end

  def cancel_reservation_response_generator do
    gen all(
          gen_status <- enum_generator(:cancelReservationStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.CancelReservationResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def certificate_signed_request_generator do
    gen all(
          gen_cert_chain <- string_generator(10_000),
          gen_cert_signing_use <- enum_generator(:certificateSigningUse)
        ) do
      %M.CertificateSignedRequest{
        certificateChain: gen_cert_chain,
        certificateType: gen_cert_signing_use
      }
    end
  end

  def certificate_signed_response_generator do
    gen all(
          gen_status <- enum_generator(:certificateSignedStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.CertificateSignedResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def clear_cache_request_generator do
    gen all(_ <- nil) do
      %M.ClearCacheRequest{}
    end
  end

  def clear_cache_response_generator do
    gen all(
          gen_status <- enum_generator(:clearCacheStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.ClearCacheResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def clear_charging_profile_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_clear_profile <- clear_charging_profile_generator()
        ) do
      %M.ClearChargingProfileRequest{
        chargingProfileId: gen_id,
        chargingProfileCriteria: gen_clear_profile
      }
    end
  end

  def clear_charging_profile_response_generator do
    gen all(
          gen_status <- enum_generator(:clearChargingProfileStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.ClearChargingProfileResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def clear_display_message_request_generator do
    gen all(gen_id <- integer_generator()) do
      %M.ClearDisplayMessageRequest{
        id: gen_id
      }
    end
  end

  def clear_display_message_response_generator do
    gen all(
          gen_status <- enum_generator(:clearMessageStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.ClearDisplayMessageResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def cleared_charging_limit_request_generator do
    gen all(
          gen_source <- enum_generator(:chargingLimitSource),
          gen_evse_id <- integer_generator()
        ) do
      %M.ClearedChargingLimitRequest{
        chargingLimitSource: gen_source,
        evseId: gen_evse_id
      }
    end
  end

  def cleared_charging_limit_response_generator do
    gen all(_ <- nil) do
      %M.ClearedChargingLimitResponse{}
    end
  end

  def clear_var_monitoring_request_generator do
    gen all(gen_id <- integer_generator()) do
      %M.ClearVariableMonitoringRequest{
        id: gen_id
      }
    end
  end

  def clear_var_monitoring_response_generator do
    gen all(gen_clear_mon_result <- clear_monitoring_result_generator()) do
      %M.ClearVariableMonitoringResponse{
        clearMonitoringResult: gen_clear_mon_result
      }
    end
  end

  def cost_updated_request_generator do
    gen all(
          gen_total_cost <- float_generator(),
          gen_transaction_id <- string_generator(36)
        ) do
      %M.CostUpdatedRequest{
        totalCost: gen_total_cost,
        transactionId: gen_transaction_id
      }
    end
  end

  def cost_updated_response_generator do
    gen all(_ <- nil) do
      %M.CostUpdatedResponse{}
    end
  end

  def customer_information_request_generator do
    gen all(
          gen_request_id <- integer_generator(),
          gen_report <- boolean_generator(),
          gen_clear <- boolean_generator(),
          gen_cust_id <- string_generator(64),
          gen_id_token <- id_token_generator(),
          gen_cert_hash <- cert_hash_generator()
        ) do
      %M.CustomerInformationRequest{
        requestId: gen_request_id,
        report: gen_report,
        clear: gen_clear,
        customerIdentifier: gen_cust_id,
        idToken: gen_id_token,
        customerCertificate: gen_cert_hash
      }
    end
  end

  def datatransfer_request_generator do
    gen all(
          gen_message_id <- string_generator(50),
          gen_data <- binary_generator(),
          gen_vendor_id <- string_generator(255)
        ) do
      %M.DataTransferRequest{
        messageId: gen_message_id,
        data: gen_data,
        vendorId: gen_vendor_id
      }
    end
  end

  def datatransfer_response_generator do
    gen all(
          gen_status <- enum_generator(:dataTransferStatus),
          gen_data <- binary_generator(),
          gen_status_info <- status_info_generator()
        ) do
      %M.DataTransferResponse{
        status: gen_status,
        data: gen_data,
        statusInfo: gen_status_info
      }
    end
  end

  def customer_information_response_generator do
    gen all(
          gen_status <- enum_generator(:customerInformationStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.CustomerInformationResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def delete_certificate_request_generator do
    gen all(gen_cert_hash <- cert_hash_generator()) do
      %M.DeleteCertificateRequest{
        certificateHashData: gen_cert_hash
      }
    end
  end

  def delete_certificate_response_generator do
    gen all(
          gen_status <- enum_generator(:deleteCertificateStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.DeleteCertificateResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def firmware_status_notification_request_generator do
    gen all(
          gen_status <- enum_generator(:firmwareStatus),
          gen_request_id <- integer_generator()
        ) do
      %M.FirmwareStatusNotificationRequest{
        status: gen_status,
        requestId: gen_request_id
      }
    end
  end

  def firmware_status_notification_response_generator do
    gen all(_ <- nil) do
      %M.FirmwareStatusNotificationResponse{}
    end
  end

  def get_15118_ev_certificate_request_generator do
    gen all(
          gen_schema_version <- string_generator(50),
          gen_action <- enum_generator(:certificateAction),
          gen_exi_request <- string_generator(5600)
        ) do
      %M.Get15118EVCertificateRequest{
        iso15118SchemaVersion: gen_schema_version,
        action: gen_action,
        exiRequest: gen_exi_request
      }
    end
  end

  def get_15118_ev_certificate_response_generator do
    gen all(
          gen_status <- enum_generator(:iso15118EVCertificateStatus),
          gen_exi_response <- string_generator(5600),
          gen_status_info <- status_info_generator()
        ) do
      %M.Get15118EVCertificateResponse{
        status: gen_status,
        exiResponse: gen_exi_response,
        statusInfo: gen_status_info
      }
    end
  end

  def get_base_report_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_report_base <- enum_generator(:reportBase)
        ) do
      %M.GetBaseReportRequest{
        requestId: gen_id,
        reportBase: gen_report_base
      }
    end
  end

  def get_base_report_response_generator do
    gen all(
          gen_status <- enum_generator(:genericDeviceModelStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetBaseReportResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def get_certificate_status_request_generator do
    gen all(gen_ocsp_data <- ocsp_req_data_generator()) do
      %M.GetCertificateStatusRequest{
        ocspRequestData: gen_ocsp_data
      }
    end
  end

  def get_certificate_status_response_generator do
    gen all(
          gen_status <- enum_generator(:getCertificateStatus),
          gen_ocsp_result <- string_generator(5500),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetCertificateStatusResponse{
        status: gen_status,
        ocspResult: gen_ocsp_result,
        statusInfo: gen_status_info
      }
    end
  end

  def get_charging_profile_request_generator do
    gen all(
          gen_req_id <- integer_generator(),
          gen_evse_id <- integer_generator(),
          gen_profile <- charging_profile_criterion_generator()
        ) do
      %M.GetChargingProfilesRequest{
        requestId: gen_req_id,
        evseId: gen_evse_id,
        chargingProfile: gen_profile
      }
    end
  end

  def get_charging_profile_response_generator do
    gen all(
          gen_status <- enum_generator(:getChargingProfileStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetChargingProfilesResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def get_composite_schedule_request_generator do
    gen all(
          gen_duration <- integer_generator(),
          gen_charging_rate_unit <- enum_generator(:chargingRateUnit),
          gen_evse_id <- integer_generator()
        ) do
      %M.GetCompositeScheduleRequest{
        duration: gen_duration,
        chargingRateUnit: gen_charging_rate_unit,
        evseId: gen_evse_id
      }
    end
  end

  def get_composite_schedule_response_generator do
    gen all(
          gen_status <- enum_generator(:genericStatus),
          gen_schedule <- composite_schedule_generator(),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetCompositeScheduleResponse{
        status: gen_status,
        schedule: gen_schedule,
        statusInfo: gen_status_info
      }
    end
  end

  def get_display_messages_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_request_id <- integer_generator(),
          gen_priority <- enum_generator(:messagePriority),
          gen_state <- enum_generator(:messageState)
        ) do
      %M.GetDisplayMessagesRequest{
        id: gen_id,
        requestId: gen_request_id,
        priority: gen_priority,
        state: gen_state
      }
    end
  end

  def get_display_messages_response_generator do
    gen all(
          gen_status <- enum_generator(:getDisplayMessagesStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetDisplayMessagesResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def get_installed_certificate_ids_request_generator do
    gen all(gen_cert_type <- enum_generator(:getCertificateIdUse)) do
      %M.GetInstalledCertificateIdsRequest{
        certificateType: gen_cert_type
      }
    end
  end

  def get_installed_certificate_ids_response_generator do
    gen all(
          gen_status <- enum_generator(:getInstalledCertificateStatus),
          gen_cert_hash_data_chain <- list_generator(cert_hash_data_chain_generator(), length: 5),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetInstalledCertificateIdsResponse{
        status: gen_status,
        certificateHashDataChain: gen_cert_hash_data_chain,
        statusInfo: gen_status_info
      }
    end
  end

  def get_local_list_version_request_generator do
    gen all(_ <- nil) do
      %M.GetLocalListVersionRequest{}
    end
  end

  def get_local_list_version_response_generator do
    gen all(gen_version <- integer_generator()) do
      %M.GetLocalListVersionResponse{
        versionNumber: gen_version
      }
    end
  end

  def get_log_request_generator do
    gen all(
          gen_log_type <- enum_generator(:log),
          gen_request_id <- integer_generator(),
          gen_retries <- integer_generator(),
          gen_interval <- integer_generator(),
          gen_log <- log_parameters_generator()
        ) do
      %M.GetLogRequest{
        logType: gen_log_type,
        requestId: gen_request_id,
        retries: gen_retries,
        retryInterval: gen_interval,
        log: gen_log
      }
    end
  end

  def get_log_response_generator do
    gen all(
          gen_status <- enum_generator(:logStatus),
          gen_filename <- string_generator(255),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetLogResponse{
        status: gen_status,
        filename: gen_filename,
        statusInfo: gen_status_info
      }
    end
  end

  def get_monitoring_report_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_criteria <- enum_generator(:monitoringCriterion),
          gen_comp_var <- component_variable_generator()
        ) do
      %M.GetMonitoringReportRequest{
        requestId: gen_id,
        monitorCriteria: gen_criteria,
        componentVariable: gen_comp_var
      }
    end
  end

  def get_monitoring_report_response_generator do
    gen all(
          gen_status <- enum_generator(:genericDeviceModelStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetMonitoringReportResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def get_report_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_comp_crit <- list_generator(enum_generator(:componentCriterion), length: 4),
          gen_comp_var <- component_variable_generator()
        ) do
      %M.GetReportRequest{
        requestId: gen_id,
        componentCriteria: gen_comp_crit,
        componentVariable: gen_comp_var
      }
    end
  end

  def get_report_response_generator do
    gen all(
          gen_status <- enum_generator(:genericDeviceModelStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.GetReportResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def get_transaction_status_request_generator do
    gen all(gen_trans_id <- string_generator(36)) do
      %M.GetTransactionStatusRequest{
        transactionId: gen_trans_id
      }
    end
  end

  def get_transaction_status_response_generator do
    gen all(
          gen_ongoing <- boolean_generator(),
          gen_in_queue <- boolean_generator()
        ) do
      %M.GetTransactionStatusResponse{
        ongoingIndicator: gen_ongoing,
        messagesInQueue: gen_in_queue
      }
    end
  end

  def get_variables_request_generator do
    gen all(
          gen_var_data <-
            list_generator(get_variable_data_generator(), min_length: 1, max_length: 5)
        ) do
      %M.GetVariablesRequest{
        getVariableData: gen_var_data
      }
    end
  end

  def get_variables_response_generator do
    gen all(
          gen_var_result <-
            list_generator(get_variable_result_generator(), min_length: 1, max_length: 5)
        ) do
      %M.GetVariablesResponse{
        getVariableResult: gen_var_result
      }
    end
  end

  def heartbeat_request_generator do
    gen all(_ <- nil) do
      %M.HeartbeatRequest{}
    end
  end

  def heartbeat_response_generator do
    gen all(gen_datetime <- date_time_generator()) do
      %M.HeartbeatResponse{currentTime: gen_datetime}
    end
  end

  def install_certificate_request_generator do
    gen all(
          gen_cert_type <- enum_generator(:installCertificateUse),
          gen_cert <- string_generator(5500)
        ) do
      %M.InstallCertificateRequest{
        certificateType: gen_cert_type,
        certificate: gen_cert
      }
    end
  end

  def meter_values_request_generator do
    gen all(
          gen_evse_id <- integer(),
          gen_meter_values_type <- list_generator(meter_value_type_generator(), length: 3)
        ) do
      %M.MeterValuesRequest{
        evseId: gen_evse_id,
        meterValue: gen_meter_values_type
      }
    end
  end

  def install_certificate_response_generator do
    gen all(
          gen_status <- enum_generator(:installCertificateStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.InstallCertificateResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def log_status_notification_request_generator do
    gen all(
          gen_status <- enum_generator(:uploadLogStatus),
          gen_request_id <- integer_generator()
        ) do
      %M.LogStatusNotificationRequest{
        status: gen_status,
        requestId: gen_request_id
      }
    end
  end

  def log_status_notification_response_generator do
    gen all(_ <- nil) do
      %M.LogStatusNotificationResponse{}
    end
  end

  def meter_values_response_generator do
    gen all(_ <- nil) do
      %M.MeterValuesResponse{}
    end
  end

  def notify_charging_limit_request_generator do
    gen all(
          gen_evse_id <- integer_generator(),
          gen_limit <- charging_limit_generator(),
          gen_schedule <- charging_schedule_generator()
        ) do
      %M.NotifyChargingLimitRequest{
        evseId: gen_evse_id,
        chargingLimit: gen_limit,
        chargingSchedule: gen_schedule
      }
    end
  end

  def notify_charging_limit_response_generator do
    gen all(_ <- nil) do
      %M.NotifyChargingLimitResponse{}
    end
  end

  def notify_customer_information_request_generator do
    gen all(
          gen_data <- string_generator(512),
          gen_tbc <- boolean_generator(),
          gen_seqno <- integer_generator(),
          gen_generated_at <- date_time_generator(),
          gen_request_id <- integer_generator()
        ) do
      %M.NotifyCustomerInformationRequest{
        data: gen_data,
        tbc: gen_tbc,
        seqNo: gen_seqno,
        generatedAt: gen_generated_at,
        requestId: gen_request_id
      }
    end
  end

  def notify_customer_information_response_generator do
    gen all(_ <- nil) do
      %M.NotifyCustomerInformationResponse{}
    end
  end

  def notify_display_messages_request_generator do
    gen all(
          gen_req_id <- integer_generator(),
          gen_tbc <- boolean_generator(),
          gen_message_info <- list_generator(message_info_generator())
        ) do
      %M.NotifyDisplayMessagesRequest{
        requestId: gen_req_id,
        tbc: gen_tbc,
        messageInfo: gen_message_info
      }
    end
  end

  def notify_display_messages_response_generator do
    gen all(_ <- nil) do
      %M.NotifyDisplayMessagesResponse{}
    end
  end

  def notify_ev_charging_needs_request_generator do
    gen all(
          gen_max_scheduled_tuples <- integer_generator(),
          gen_evse_id <- integer_generator(),
          gen_needs <- charging_needs_generator()
        ) do
      %M.NotifyEVChargingNeedsRequest{
        maxScheduleTuples: gen_max_scheduled_tuples,
        evseId: gen_evse_id,
        chargingNeeds: gen_needs
      }
    end
  end

  def notify_ev_charging_needs_response_generator do
    gen all(
          gen_status <- enum_generator(:notifyEVChargingNeedsStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.NotifyEVChargingNeedsResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def notify_ev_charging_schedule_request_generator do
    gen all(
          gen_timebase <- date_time_generator(),
          gen_evse_id <- integer_generator(),
          gen_schedule <- charging_schedule_generator()
        ) do
      %M.NotifyEVChargingScheduleRequest{
        timeBase: gen_timebase,
        evseId: gen_evse_id,
        chargingSchedule: gen_schedule
      }
    end
  end

  def notify_ev_charging_schedule_response_generator do
    gen all(_ <- nil) do
      %M.NotifyEVChargingScheduleResponse{}
    end
  end

  def notify_event_request_generator do
    gen all(
          gen_generated_at <- date_time_generator(),
          gen_tbc <- boolean_generator(),
          gen_seq_no <- integer_generator(),
          gen_event_data <- list_generator(event_data_generator(), length: 5)
        ) do
      %M.NotifyEventRequest{
        generatedAt: gen_generated_at,
        tbc: gen_tbc,
        seqNo: gen_seq_no,
        eventData: gen_event_data
      }
    end
  end

  def notify_event_response_generator do
    gen all(_ <- nil) do
      %M.NotifyEventResponse{}
    end
  end

  def notify_monitoring_report_request_generator do
    gen all(
          gen_req_id <- integer_generator(),
          gen_tbc <- boolean_generator(),
          gen_seq_no <- integer_generator(),
          gen_gen_at <- date_time_generator(),
          gen_mon <- list_generator(monitoring_data_generator(), length: 5)
        ) do
      %M.NotifyMonitoringReportRequest{
        requestId: gen_req_id,
        tbc: gen_tbc,
        seqNo: gen_seq_no,
        generatedAt: gen_gen_at,
        monitoring: gen_mon
      }
    end
  end

  def notify_monitoring_report_response_generator do
    gen all(_ <- nil) do
      %M.NotifyMonitoringReportResponse{}
    end
  end

  def notify_report_request_generator do
    gen all(
          gen_req_id <- integer_generator(),
          gen_gen_at <- date_time_generator(),
          gen_tbc <- boolean_generator(),
          gen_seq_no <- integer_generator(),
          gen_data <- report_data_generator()
        ) do
      %M.NotifyReportRequest{
        requestId: gen_req_id,
        generatedAt: gen_gen_at,
        tbc: gen_tbc,
        seqNo: gen_seq_no,
        reportData: gen_data
      }
    end
  end

  def notify_report_response_generator do
    gen all(_ <- nil) do
      %M.NotifyReportResponse{}
    end
  end

  def publish_firmware_request_generator do
    gen all(
          gen_loc <- string_generator(512),
          gen_retries <- integer_generator(),
          gen_checksum <- string_generator(32),
          gen_req_id <- integer_generator(),
          gen_retry_interval <- integer_generator()
        ) do
      %M.PublishFirmwareRequest{
        location: gen_loc,
        retries: gen_retries,
        checksum: gen_checksum,
        requestId: gen_req_id,
        retryInterval: gen_retry_interval
      }
    end
  end

  def publish_firmware_response_generator do
    gen all(
          gen_status <- enum_generator(:genericStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.PublishFirmwareResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def publish_firmware_status_notification_request_generator do
    gen all(
          gen_status <- enum_generator(:publishFirmwareStatus),
          gen_location <- string_generator(512),
          gen_req_id <- integer_generator()
        ) do
      %M.PublishFirmwareStatusNotificationRequest{
        status: gen_status,
        location: gen_location,
        requestId: gen_req_id
      }
    end
  end

  def publish_firmware_status_notification_response_generator do
    gen all(_ <- nil) do
      %M.PublishFirmwareStatusNotificationResponse{}
    end
  end

  def report_charging_profiles_request_generator do
    gen all(
          gen_req_id <- integer_generator(),
          gen_charging_limit_source <- enum_generator(:chargingLimitSource),
          gen_tbc <- boolean_generator(),
          gen_evse_id <- integer_generator(),
          gen_charging_profiles <-
            list_generator(charging_profile_generator(), minLength: 1, maxLength: 5)
        ) do
      %M.ReportChargingProfilesRequest{
        requestId: gen_req_id,
        chargingLimitSource: gen_charging_limit_source,
        tbc: gen_tbc,
        evseId: gen_evse_id,
        chargingProfile: gen_charging_profiles
      }
    end
  end

  def report_charging_profiles_response_generator do
    gen all(_ <- nil) do
      %M.ReportChargingProfilesResponse{}
    end
  end

  def request_start_transaction_request_generator do
    gen all(
          gen_evse_id <- integer_generator(),
          gen_remote_id <- integer_generator(),
          gen_id_token <- id_token_generator(),
          gen_profile <- charging_profile_generator(),
          gen_group_token <- id_token_generator()
        ) do
      %M.RequestStartTransactionRequest{
        evseId: gen_evse_id,
        remoteStartId: gen_remote_id,
        idToken: gen_id_token,
        chargingProfile: gen_profile,
        groupIdToken: gen_group_token
      }
    end
  end

  def request_start_transaction_response_generator do
    gen all(
          gen_status <- enum_generator(:requestStartStopStatus),
          gen_trans_id <- string_generator(36),
          gen_status_info <- status_info_generator()
        ) do
      %M.RequestStartTransactionResponse{
        status: gen_status,
        transactionId: gen_trans_id,
        statusInfo: gen_status_info
      }
    end
  end

  def request_stop_transaction_request_generator do
    gen all(gen_trans_id <- string_generator(36)) do
      %M.RequestStopTransactionRequest{
        transactionId: gen_trans_id
      }
    end
  end

  def request_stop_transaction_response_generator do
    gen all(_ <- nil) do
      %M.RequestStopTransactionResponse{}
    end
  end

  def reservation_status_update_request_generator do
    gen all(
          gen_res_id <- integer_generator(),
          gen_res_update_status <- enum_generator(:reservationUpdateStatus)
        ) do
      %M.ReservationStatusUpdateRequest{
        reservationId: gen_res_id,
        reservationUpdateStatus: gen_res_update_status
      }
    end
  end

  def reservation_status_update_response_generator do
    gen all(_ <- nil) do
      %M.ReservationStatusUpdateResponse{}
    end
  end

  def reserve_now_request_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_expiry <- date_time_generator(),
          gen_con_type <- enum_generator(:connectorType),
          gen_evse_id <- integer_generator(),
          gen_id_token <- id_token_generator(),
          gen_group_token <- id_token_generator()
        ) do
      %M.ReserveNowRequest{
        id: gen_id,
        expiryDateTime: gen_expiry,
        connectorType: gen_con_type,
        evseId: gen_evse_id,
        idToken: gen_id_token,
        groupIdToken: gen_group_token
      }
    end
  end

  def reserve_now_response_generator do
    gen all(
          gen_status <- enum_generator(:reserveNowStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.ReserveNowResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def reset_request_generator do
    gen all(
          gen_type <- enum_generator(:reset),
          gen_evse_id <- integer_generator()
        ) do
      %M.ResetRequest{
        type: gen_type,
        evseId: gen_evse_id
      }
    end
  end

  def reset_response_generator do
    gen all(
          gen_status <- enum_generator(:resetStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.ResetResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def security_event_notification_request_generator do
    gen all(
          gen_type <- string_generator(50),
          gen_timestamp <- date_time_generator(),
          gen_info <- string_generator(255)
        ) do
      %M.SecurityEventNotificationRequest{
        type: gen_type,
        timestamp: gen_timestamp,
        techIndo: gen_info
      }
    end
  end

  def security_event_notification_response_generator do
    gen all(_ <- nil) do
      %M.SecurityEventNotificationResponse{}
    end
  end

  def send_local_list_request_generator do
    gen all(
          gen_version <- integer_generator(),
          gen_update_type <- enum_generator(:update),
          gen_local_auth_list <- list_generator(authorization_data_generator(), length: 4)
        ) do
      %M.SendLocalListRequest{
        versionNumber: gen_version,
        updateType: gen_update_type,
        localAuthorizationList: gen_local_auth_list
      }
    end
  end

  def send_local_list_response_generator do
    gen all(
          gen_status <- enum_generator(:sendLocalListStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.SendLocalListResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def set_charging_profile_request_generator do
    gen all(
          gen_evse_id <- integer_generator(),
          gen_charging_profile_type <- charging_profile_generator()
        ) do
      %M.SetChargingProfileRequest{
        evseId: gen_evse_id,
        chargingProfile: gen_charging_profile_type
      }
    end
  end

  def set_charging_profile_response_generator do
    gen all(
          gen_status <- enum_generator(:chargingProfileStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.SetChargingProfileResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def set_display_message_request_generator do
    gen all(gen_message <- message_info_generator()) do
      %M.SetDisplayMessageRequest{
        message: gen_message
      }
    end
  end

  def set_display_message_response_generator do
    gen all(
          gen_status <- enum_generator(:displayMessageStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.SetDisplayMessageResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end

  def status_notification_request_generator do
    gen all(
          gen_datetime <- date_time_generator(),
          gen_con_status <- enum_generator(:connectorStatus),
          gen_evse_id <- integer_generator(0..10),
          gen_conn_id <- integer_generator(0..20)
        ) do
      %M.StatusNotificationRequest{
        timestamp: gen_datetime,
        connectorStatus: gen_con_status,
        evseId: gen_evse_id,
        connectorId: gen_conn_id
      }
    end
  end

  def status_notification_response_generator do
    gen all(_ <- nil) do
      %M.StatusNotificationResponse{}
    end
  end

  def transactionevent_request_generator do
    gen all(
          gen_event_type <- enum_generator(:transactionEvent),
          gen_datetime <- date_time_generator(),
          gen_trigger_reason <- enum_generator(:triggerReason),
          gen_seq_no <- integer_generator(),
          gen_offline <- boolean_generator(),
          gen_nr_phases <- integer_generator(),
          gen_cable_max_current <- integer_generator(),
          gen_reservation_id <- integer_generator(),
          gen_transaction_info <- transaction_info_generator(),
          gen_id_token <- id_token_generator(),
          gen_evse <- evse_generator(),
          gen_meter_value_type <- list_generator(meter_value_type_generator(), length: 4)
        ) do
      %M.TransactionEventRequest{
        eventType: gen_event_type,
        timestamp: gen_datetime,
        triggerReason: gen_trigger_reason,
        seqNo: gen_seq_no,
        offline: gen_offline,
        numberOfPhasesUsed: gen_nr_phases,
        cableMaxCurrent: gen_cable_max_current,
        reservationId: gen_reservation_id,
        transactionInfo: gen_transaction_info,
        idToken: gen_id_token,
        evse: gen_evse,
        meterValue: gen_meter_value_type
      }
    end
  end

  def transactionevent_response_generator do
    gen all(
          gen_total_cost <- float_generator(),
          gen_charging_prio <- integer_generator(-9..9),
          gen_id_token_info <- id_token_info_generator(),
          gen_message <- personal_message_generator()
        ) do
      %M.TransactionEventResponse{
        totalCost: gen_total_cost,
        chargingPriority: gen_charging_prio,
        idTokenInfo: gen_id_token_info,
        updatedPersonalMessage: gen_message
      }
    end
  end

  def trigger_message_request_generator do
    gen all(
          gen_req_message <- enum_generator(:messageTrigger),
          gen_evse <- evse_generator()
        ) do
      %M.TriggerMessageRequest{
        requestedMessage: gen_req_message,
        evse: gen_evse
      }
    end
  end

  def trigger_message_response_generator do
    gen all(
          gen_status <- enum_generator(:triggerMessageStatus),
          gen_status_info <- status_info_generator()
        ) do
      %M.TriggerMessageResponse{
        status: gen_status,
        statusInfo: gen_status_info
      }
    end
  end
end
