# OCPP v1.6 Messages

List of OCPP 1.6 Messages, Data Types and Enumerations which are implemented in this library

- [OCPP v1.6 Messages](#ocpp-v16-messages)
  - [how to implement a message](#how-to-implement-a-message)
  - [Messages](#messages)
- [Datatypes and Enumerations](#datatypes-and-enumerations)

## how to implement a message

* Add Request and Response structs to `/lib/ocpp_model/v16/messages.ex`
* Add any new DataTypes to `/lib/ocpp_model/v16/datatypes.ex`
* Add any new EnumTypes to `/lib/ocpp_model/v16/enumtypes.ex`
* Add property generators to the correct `/test/generators/*` modules and the property tests to `test/ocpp_model_messages_test.ex` 
* Add a callback and handle clause to the right `/lib/ocpp_model/v16/behaviours/*`
* Implement the behaviour in either the test Charger or ChargeSystem and write a test for it in either `test/ocpp_charger_test.ex` or `test/ocpp_chargesystem_test.ex`
* run `MIX_ENV=test mix all` for any test, coverage or linting issues

## Messages
Ch = Chargestation, CS = ChargerSystem

| Direction | Message                       | Implemented |
| --------- | ----------------------------- | ----------- |
| Ch -> CS  | Authorize                     | yes         |
| Ch -> CS  | BootNotification              | yes         |
| CS -> Ch  | CancelReservation             | no          |
| CS -> Ch  | ChangeAvailability            | no          |
| CS -> Ch  | ChangeConfiguration           | no          |
| CS -> Ch  | ClearCache                    | no          |
| CS -> Ch  | ClearChargingProfile          | no          |
| Ch <> CS  | DataTransfer                  | no          |
| Ch -> CS  | DiagnosticsStatusNotification | no          |
| Ch -> CS  | FirmwareStatusNotification    | no          |
| CS -> Ch  | GetCompositeSchedule          | no          |
| CS -> Ch  | GetConfiguration              | no          |
| CS -> Ch  | GetDiagnostics                | no          |
| CS -> Ch  | GetLocalListVersion           | no          |
| Ch -> CS  | Heartbeat                     | yes         |
| Ch -> CS  | MeterValues                   | no          |
| CS -> Ch  | RemoteStartTransaction        | no          |
| CS -> Ch  | RemoteStopTransaction         | no          |
| CS -> Ch  | ReserveNow                    | no          |
| CS -> Ch  | Reset                         | yes         |
| CS -> Ch  | SendLocalList                 | no          |
| CS -> Ch  | SetChargingProfile            | no          |
| Ch -> CS  | StartTransaction              | yes         |
| Ch -> CS  | StatusNotification            | yes         |
| Ch -> CS  | StopTransaction               | yes         |
| CS -> Ch  | TriggerMessage                | no          |
| CS -> Ch  | UnlockConnector               | no          |
| CS -> Ch  | UpdateFirmware                | no          |

# Datatypes and Enumerations

| DataType                   | Implemented |
| -------------------------- | ----------- |
| AuthorizationData          | yes         |
| AuthorizationStatus        | yes         |
| AvailabilityStatus         | no          |
| AvailabilityType           | no          |
| CancelReservationStatus    | no          |
| ChargePointErrorCode       | yes         |
| ChargePointStatus          | yes         |
| ChargingProfile            | no          |
| ChargingProfileKindType    | no          |
| ChargingProfilePurposeType | no          |
| ChargingProfileStatus      | no          |
| ChargingRateUnitType       | no          |
| ChargingSchedule           | no          |
| ChargingSchedulePeriod     | no          |
| ClearCacheStatus           | no          |
| ClearChargingProfileStatus | no          |
| ConfigurationStatus        | no          |
| DataTransferStatus         | no          |
| DiagnosticsStatus          | no          |
| FirmwareStatus             | no          |
| GetCompositeScheduleStatus | no          |
| IdTagInfo                  | yes         |
| IdToken                    | yes         | * treated as a String
| KeyValue                   | no          |
| Location                   | yes         |
| Measurand                  | yes         |
| MessageTrigger             | no          |
| MeterValue                 | yes         |
| Phase                      | yes         |
| ReadingContext             | yes         |
| Reason                     | yes         |
| RecurrencyKindType         | no          |
| RegistrationStatus         | yes         |
| RemoteStartStopStatus      | no          |
| ReservationStatus          | no          |
| ResetStatus                | yes         |
| ResetType                  | yes         |
| SampledValue               | yes         |
| TriggerMessageStatus       | no          |
| UnitOfMeasure              | yes         |
| UnlockStatus               | no          |
| UpdateStatus               | no          |
| UpdateType                 | no          |
| ValueFormat                | yes         |
