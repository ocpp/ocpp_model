defmodule Implementations.V16.MyTestBasicChargeSystem do
  @moduledoc """
    Basic chargesystem implementation, only supports the minimum required to do a chargesession.
  """

  alias OcppModel.V16.Behaviours, as: B
  alias OcppModel.V16.DataTypes, as: DT
  # alias OcppModel.V16.EnumTypes, as: ET
  alias OcppModel.V16.Messages, as: M

  @behaviour B.BasicChargeSystem

  def handle([2, id, action, payload], state) do
    case B.BasicChargeSystem.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, state} -> {[4, id, Atom.to_string(error), desc, {}], state}
    end
  end

  def handle({[3, id, payload], _state}),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle({[4, id, err, desc, det], _state}),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  @impl B.BasicChargeSystem
  def authorize(_req, state) do
    {{:ok, %M.AuthorizeResponse{idTagInfo: %DT.IdTagInfo{status: "Accepted"}}}, state}
  end

  @impl B.BasicChargeSystem
  def boot_notification(_req, state) do
    {{:ok,
      %M.BootNotificationResponse{
        currentTime: current_time(),
        interval: 900,
        status: "Accepted"
      }}, state}
  end

  @impl B.BasicChargeSystem
  def heartbeat(_req, state) do
    {{:ok, %M.HeartbeatResponse{currentTime: current_time()}}, state}
  end

  @impl B.BasicChargeSystem
  def start_transaction(_req, state) do
    {{:ok, %M.StartTransactionResponse{
      idTagInfo: %DT.IdTagInfo{status: "Accepted"},
      transactionId: 42}}, state}
  end

  @impl B.BasicChargeSystem
  def status_notification(_req, state) do
    {{:ok, %M.StatusNotificationResponse{}}, state}
  end

  @impl B.BasicChargeSystem
  def stop_transaction(_req, state) do
    {{:ok, %M.StopTransactionResponse{idTagInfo: %DT.IdTagInfo{status: "Accepted"}}}, state}
  end

  def current_time, do: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601()
end
