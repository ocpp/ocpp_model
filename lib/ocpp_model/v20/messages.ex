defmodule OcppModel.V20.Messages do
  @moduledoc """
    OCPP 2.0 message structs
  """
  alias OcppModel.V20.DataTypes, as: DT

  defmodule AuthorizeRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:certificate, String.t())
      field(:idToken, DT.IdToken.t(), enforce: true)
      field(:iso15118CertificateHashData, list(OCSPRequestData.t()))
    end
  end

  defmodule AuthorizeResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # AuthorizeCertificateStatusEnumType
      field(:certificateStatus, String.t())
      field(:idTokenInfo, DT.IdTokenInfo.t(), enforce: true)
    end
  end

  defmodule BootNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # BootReasonEnumType
      field(:reason, String.t(), enforce: true)
      field(:chargingStation, DT.ChargingStation.t(), enforce: true)
    end
  end

  defmodule BootNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # dateTime
      field(:currentTime, String.t(), enforce: true)
      field(:interval, integer(), enforce: true)
      # RegistrationStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, StatusInfo.t())
    end
  end

  defmodule CancelReservationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:reservationId, integer(), enforce: true)
    end
  end

  defmodule CancelReservationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # CancelReservationStatusEnumType
      field(:status, String.t(), enforce: true)
      # StatusInfoType
      field(:statusInfo, String.t())
    end
  end

  defmodule CertificateSignedRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:certificateChain, String.t(), enforce: true)
      # CertificateSigningUseEnumType
      field(:certificateType, String.t())
    end
  end

  defmodule CertificateSignedResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:status, String.t(), enforce: true)
      # StatusInfoType
      field(:statusInfo, String.t())
    end
  end

  defmodule ChangeAvailabilityRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # OperationalStatusEnumType
      field(:operationalStatus, String.t(), enforce: true)
      field(:evse, Evse.t())
    end
  end

  defmodule ChangeAvailabilityResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ChangeAvailabilityStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule ClearCacheRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule ClearCacheResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ClearCacheStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule ClearChargingProfileRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:chargingProfileId, integer())
      # ClearChargingProfileType
      field(:chargingProfileCriteria, String.t())
    end
  end

  defmodule ClearChargingProfileResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ClearChargingProfileStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule ClearDisplayMessageRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
    end
  end

  defmodule ClearDisplayMessageResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ClearMessageStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule ClearedChargingLimitRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ChargingLimitSourceEnumType
      field(:chargingLimitSource, String.t(), enforce: true)
      field(:evseId, integer())
    end
  end

  defmodule ClearedChargingLimitResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule ClearVariableMonitoringRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, list(integer()), enforce: true)
    end
  end

  defmodule ClearVariableMonitoringResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:clearMonitoringResult, list(DT.ClearMonitoringResult.t()), enforce: true)
    end
  end

  defmodule CostUpdatedRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:totalCost, float(), enforce: true)
      field(:transactionId, String.t(), enforce: true)
    end
  end

  defmodule CostUpdatedResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule CustomerInformationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      field(:report, boolean(), enforce: true)
      field(:clear, boolean(), enforce: true)
      field(:customerIdentifier, String.t())
      field(:idToken, DT.IdToken.t())
      field(:customerCertificate, DT.CertificateHashData.t())
    end
  end

  defmodule CustomerInformationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # CustomerInformationStatusEnumType
      field(:status, String.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule DataTransferRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # 0..50
      field(:messageId, String.t())
      # anytype
      field(:data, String.t(), enforce: true)
      # 0..255
      field(:vendorId, String.t())
    end
  end

  defmodule DataTransferResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # DataTransferStatusEnumType
      field(:status, String.t(), enforce: true)
      # any type
      field(:data, String.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule DeleteCertificateRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:certificateHashData, DT.CertificateHashData.t(), enforce: true)
    end
  end

  defmodule DeleteCertificateResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # DeleteCertificateStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule FirmwareStatusNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # FirmwareStatus EnumType
      field(:status, String.t(), enforce: true)
      field(:requestId, integer())
    end
  end

  defmodule FirmwareStatusNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule Get15118EVCertificateRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:iso15118SchemaVersion, String.t(), enforce: true)
      field(:action, String.t(), enforce: true)
      field(:exiRequest, String.t(), enforce: true)
    end
  end

  defmodule Get15118EVCertificateResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # Iso15118EVCertificateStatus EnumType
      field(:status, String.t(), enforce: true)
      field(:exiResponse, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetChargingProfilesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      field(:evseId, integer())
      field(:chargingProfile, DT.ChargingProfileCriterion.t())
    end
  end

  defmodule GetChargingProfilesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetChargingProfileStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetBaseReportRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      # ReportBaseEnumType
      field(:reportBase, String.t(), enforce: true)
    end
  end

  defmodule GetBaseReportResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GenericDeviceModuleStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetCertificateStatusRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:ocspRequestData, DT.OCSPRequestData.t(), enforce: true)
    end
  end

  defmodule GetCertificateStatusResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:status, String.t(), enforce: true)
      field(:ocspResult, String.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetCompositeScheduleRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:duration, integer(), enforce: true)
      # ChargingRateUnit EnumType
      field(:chargingRateUnit, String.t())
      field(:evseId, integer(), enforce: true)
    end
  end

  defmodule GetCompositeScheduleResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GenericStatus EnumType
      field(:status, String.t(), enforce: true)
      field(:schedule, DT.CompositeSchedule.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetDisplayMessagesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, list(integer()))
      field(:requestId, integer(), enforce: true)
      # MessagePriorityEnumType
      field(:priority, String.t())
      # MessageStateEnumType
      field(:state, String.t())
    end
  end

  defmodule GetDisplayMessagesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetDisplayMessageStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetInstalledCertificateIdsRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetCertificateIdUseEnumType
      field(:certificateType, list(String.t()))
    end
  end

  defmodule GetInstalledCertificateIdsResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GetInstalledCertificateStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:certificateHashDataChain, list(DT.CertificateHashDataChain))
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetLocalListVersionRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule GetLocalListVersionResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:versionNumber, integer(), enforce: true)
    end
  end

  defmodule GetLogRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # LogEnumType
      field(:logType, String.t(), enforce: true)
      field(:requestId, integer(), enforce: true)
      field(:retries, integer())
      field(:retryInterval, integer())
      field(:log, DT.LogParameters.t(), enforce: true)
    end
  end

  defmodule GetLogResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # LogStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:filename, String.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetMonitoringReportRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      # MonitorCriterionEnumType
      field(:monitorCriteria, String.t())
      field(:componentVariable, DT.ComponentVariable.t())
    end
  end

  defmodule GetMonitoringReportResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GenericDeviceModelStatus
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetReportRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      # ComponentCriterionEnumType
      field(:componentCriteria, list(String.t()))
      field(:componentVariable, DT.ComponentVariable.t())
    end
  end

  defmodule GetReportResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GenericDeviceModelStatus
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule GetTransactionStatusRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:transactionId, String.t(), enforce: true)
    end
  end

  defmodule GetTransactionStatusResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:ongoingIndicator, boolean())
      field(:messagesInQueue, boolean())
    end
  end

  defmodule GetVariablesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:getVariableData, list(DT.GetVariableData.t()), enforce: true)
    end
  end

  defmodule GetVariablesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:getVariableResult, list(DT.GetVariableResult.t()), enforce: true)
    end
  end

  defmodule HeartbeatRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # no fields
    end
  end

  defmodule HeartbeatResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:currentTime, String.t(), enforce: true)
    end
  end

  defmodule InstallCertificateRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # InstallCertificateUseEnumType
      field(:certificateType, String.t(), enforce: true)
      field(:certificate, String.t(), enforce: true)
    end
  end

  defmodule InstallCertificateResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # InstallCertificateStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule LogStatusNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # UploadLogStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:requestId, integer())
    end
  end

  defmodule LogStatusNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule MeterValuesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer(), enforce: true)
      field(:meterValue, list(DT.MeterValue.t()), enforce: true)
    end
  end

  defmodule MeterValuesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyChargingLimitRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer())
      field(:chargingLimit, DT.ChargingLimit.t(), enforce: true)
      field(:chargingSchedule, DT.ChargingSchedule.t())
    end
  end

  defmodule NotifyChargingLimitResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyCustomerInformationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:data, String.t(), enforce: true)
      field(:tbc, boolean())
      field(:seqNo, integer(), enforce: true)
      field(:generatedAt, String.t(), enforce: true)
      field(:requestId, integer(), enforce: true)
    end
  end

  defmodule NotifyCustomerInformationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyDisplayMessagesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      field(:tbc, boolean())
      field(:messageInfo, list(DT.MessageInfo.t()))
    end
  end

  defmodule NotifyDisplayMessagesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyEVChargingNeedsRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:maxScheduleTuples, integer())
      field(:evseId, integer, enforce: true)
      field(:chargingNeeds, DT.ChargingNeeds.t(), enforce: true)
    end
  end

  defmodule NotifyEVChargingNeedsResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # NotifyEVChargingNeedsStatue
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule NotifyEVChargingScheduleRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:timeBase, String.t(), enforce: true)
      field(:evseId, integer(), enforce: true)
      field(:chargingSchedule, DT.ChargingSchedule.t(), enforce: true)
    end
  end

  defmodule NotifyEVChargingScheduleResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyEventRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:generatedAt, String.t(), enforce: true)
      field(:tbc, boolean())
      field(:seqNo, integer(), enforce: true)
      field(:eventData, list(DT.EventData.t()), enforce: true)
    end
  end

  defmodule NotifyEventResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyMonitoringReportRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      field(:tbc, boolean())
      field(:seqNo, integer(), enforce: true)
      field(:generatedAt, String.t(), enforce: true)
      field(:monitoring, list(DT.MonitoringData.t()))
    end
  end

  defmodule NotifyMonitoringReportResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule NotifyReportRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      field(:generatedAt, String.t(), enforce: true)
      field(:tbc, boolean())
      field(:seqNo, integer(), enforce: true)
      field(:reportData, list(DT.ReportData))
    end
  end

  defmodule NotifyReportResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule PublishFirmwareRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:location, String.t(), enforce: true)
      field(:retries, integer())
      field(:checksum, String.t(), enforce: true)
      field(:requestId, integer(), enforce: true)
      field(:retryInterval, integer())
    end
  end

  defmodule PublishFirmwareResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # GenericStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule PublishFirmwareStatusNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:status, String.t(), enforce: true)
      field(:location, String.t())
      field(:requestId, String.t())
    end
  end

  defmodule PublishFirmwareStatusNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule ReportChargingProfilesRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:requestId, integer(), enforce: true)
      # ChargingLimitSource
      field(:chargingLimitSource, String.t(), enforce: true)
      field(:tbc, boolean())
      field(:evseId, integer(), enforce: true)
      field(:chargingProfile, list(DT.ChargingProfile.t()), enforce: true)
    end
  end

  defmodule ReportChargingProfilesResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule RequestStartTransactionRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer(), enforce: true)
      field(:remoteStartId, integer(), enforce: true)
      field(:idToken, DT.IdToken.t(), enforce: true)
      field(:chargingProfile, DT.ChargingProfile.t())
      field(:groupIdToken, DT.IdToken.t())
    end
  end

  defmodule RequestStartTransactionResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # RequestStartStopStatus
      field(:status, String.t(), enforce: true)
      field(:transactionId, String.t())
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule RequestStopTransactionRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:transactionId, String.t(), enforce: true)
    end
  end

  defmodule RequestStopTransactionResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule ReservationStatusUpdateRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:reservationId, integer(), enforce: true)
      # ReservationUpdateStatusEnumType
      field(:reservationUpdateStatus, String.t(), enforce: true)
    end
  end

  defmodule ReservationStatusUpdateResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule ReserveNowRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:id, integer(), enforce: true)
      field(:expiryDateTime, String.t(), enforce: true)
      # ConnectorTypeEnumType
      field(:connectorType, String.t())
      field(:evseId, integer())
      field(:idToken, DT.IdToken.t(), enforce: true)
      field(:groupIdToken, DT.IdToken)
    end
  end

  defmodule ReserveNowResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ReserveNowStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule ResetRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ResetEnumType
      field(:type, String.t(), enforce: true)
      field(:evseId, integer())
    end
  end

  defmodule ResetResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ResetStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule SecurityEventNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # SecurityEventslist
      field(:type, String.t(), enforce: true)
      field(:timestamp, String.t(), enforce: true)
      field(:techIndo, String.t())
    end
  end

  defmodule SecurityEventNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule SendLocalListRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:versionNumber, integer(), enforce: true)
      # UpdateEnumType
      field(:updateType, String.t(), enforce: true)
      field(:localAuthorizationList, list(DT.AuthorizationData))
    end
  end

  defmodule SendLocalListResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # SendLocalListEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule StatusNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # dateTime
      field(:timestamp, String.t(), enforce: true)
      # ConnectorStatusEnumType
      field(:connectorStatus, String.t(), enforce: true)
      field(:evseId, integer(), enforce: true)
      field(:connectorId, integer(), enforce: true)
    end
  end

  defmodule StatusNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule SetChargingProfileRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer(), enforce: true)
      field(:chargingProfile, DT.ChargingProfile.t(), enforce: true)
    end
  end

  defmodule SetChargingProfileResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # ChargingProfileStatus
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule SetDisplayMessageRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:message, DT.MessageInfo.t(), enforce: true)
    end
  end

  defmodule SetDisplayMessageResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule TransactionEventRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # TransactionEventEnumType
      field(:eventType, String.t(), enforce: true)
      # dateTime
      field(:timestamp, String.t(), enforce: true)
      # TriggerreasonEnumType
      field(:triggerReason, String.t(), enforce: true)
      field(:seqNo, integer(), enforce: true)
      field(:offline, boolean())
      field(:numberOfPhasesUsed, integer())
      field(:cableMaxCurrent, integer())
      field(:reservationId, integer())
      field(:transactionInfo, DT.Transaction.t(), enforce: true)
      field(:idToken, DT.IdToken.t())
      field(:evse, DT.Evse.t())
      field(:meterValue, list(DT.MeterValue.t()))
    end
  end

  defmodule TransactionEventResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:totalCost, float())
      field(:chargingPriority, integer())
      field(:idTokenInfo, DT.IdTokenInfo.t())
      field(:updatedPersonalMessage, DT.MessageContent.t())
    end
  end

  defmodule TriggerMessageRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # MessageTriggerEnumType
      field(:requestedMessage, String.t(), enforce: true)
      field(:evse, DT.Evse.t())
    end
  end

  defmodule TriggerMessageResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # TriggerMessageStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end

  defmodule UnlockConnectorRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field(:evseId, integer(), enforce: true)
    end
  end

  defmodule UnlockConnectorResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      # UnlockStatusEnumType
      field(:status, String.t(), enforce: true)
      field(:statusInfo, DT.StatusInfo.t())
    end
  end
end
