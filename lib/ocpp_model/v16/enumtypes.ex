defmodule OcppModel.V16.EnumTypes do
  @moduledoc """
    Contains a map of all EnumTypes that are used in the currently supported messages, with a function to validate if a value is part of the EnumType
  """
  @enum_types %{
    authorizationStatus:  ["Accepted", "Blocked", "Expired", "Invalid", "ConcurrentTx"],
    chargePointErrorCode: ["ConnectorLockFailure", "EVCommunicationError", "GroundFailure",
                           "HighTemperature", "InternalError", "LocalListConflict", "NoError",
                           "OtherError", "OverCurrentFailure", "OverVoltage", "PowerMeterFailure",
                           "PowerSwitchFailure", "ReaderFailure", "ResetFailure", "UnderVoltage", "WeakSignal"],
    chargePointStatus:    ["Available", "Preparing", "Charging", "SuspendedEVSE", "SuspendedEV",
                           "Finishing", "Reserved", "Unavailable", "Faulted"],
    location:             ["Body", "Cable", "EV", "Inlet", "Outlet"],
    measurand:            ["Current.Export", "Current.Import", "Current.Offered",
                           "Energy.Active.Export.Register", "Energy.Active.Import.Register"],
    phase:                ["L1", "L2", "L3", "N", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1"],
    readingContext:       ["Interruption.Begin", "Interruption.End", "Other", "Sample.Clock",
                           "Sample.Periodic", "Transaction.Begin"],
    reason:               ["DeAuthorized", "EmergencyStop", "EVDisconnected", "HardReset", "Local",
                           "Other", "PowerLoss", "Reboot", "Remote", "SoftReset", "UnlockCommand"],
    registrationStatus:   ["Accepted", "Pending", "Rejected"],
    resetStatus:          ["Accepted", "Rejected"],
    resetType:            ["Hard", "Soft"],
    unitOfMeasure:        ["Wh", "kWh", "varh", "kvarh", "W", "kW", "VA", "kVA", "var", "kvar",
                           "A", "V", "Celsius", "Fahrenheit", "K", "Percent"],
    valueFormat:          ["Raw", "SignedData"]
  }

@spec validate?(atom(), String.t()) :: boolean()
  @doc """
    Validates whether or not the value is part of the EnumType
  """
  def validate?(enum_type, value) do
    case Map.get(@enum_types, enum_type) do
      nil -> false
      values -> Enum.member?(values, value)
    end
  end

  @doc """
    Returns the entire EnumTypes Map
  """
  @spec get :: map()
  def get, do: @enum_types

  @doc """
    Returns the list of values for the specified EnumType
  """
  @spec get(atom()) :: list(String.t()) | nil
  def get(item), do: Map.get(@enum_types, item)
end
