defmodule Implementations.V20.MyTestBasicChargeSystem do
  @moduledoc """
    Basic chargesystem implementation, only supports the minimum required to do a chargesession.
  """

  alias OcppModel.V20.Behaviours, as: B
  alias OcppModel.V20.DataTypes, as: DT
  alias OcppModel.V20.EnumTypes, as: ET
  alias OcppModel.V20.Messages, as: M

  @behaviour B.BasicChargeSystem

  def handle([2, id, action, payload], state) do
    case B.BasicChargeSystem.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, state} -> {[4, id, Atom.to_string(error), desc, {}], state}
    end
  end

  def handle({[3, id, payload], _state}),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle({[4, id, err, desc, det], _state}),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  @impl B.BasicChargeSystem
  def authorize(_req, state) do
    {{:ok, %M.AuthorizeResponse{idTokenInfo: %DT.IdTokenInfo{status: "Accepted"}}}, state}
  end

  @impl B.BasicChargeSystem
  def boot_notification(req, state) do
    if ET.validate?(:bootReason, req.reason) do
      {{:ok,
        %M.BootNotificationResponse{
          currentTime: current_time(),
          interval: 900,
          status: %DT.StatusInfo{reasonCode: ""}
        }}, state}
    else
      {{:error, :boot_notification, "#{req.reason} is not a valid BootReason"}, state}
    end
  end

  @impl B.BasicChargeSystem
  def heartbeat(_req, state) do
    {{:ok, %M.HeartbeatResponse{currentTime: current_time()}}, state}
  end

  @impl B.BasicChargeSystem
  def status_notification(_req, state) do
    {{:ok, %M.StatusNotificationResponse{}}, state}
  end

  @impl B.BasicChargeSystem
  def transaction_event(_req, state) do
    {{:ok, %M.TransactionEventResponse{}}, state}
  end

  def current_time, do: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601()
end
