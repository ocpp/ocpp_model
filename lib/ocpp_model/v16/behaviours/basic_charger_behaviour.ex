defmodule OcppModel.V16.Behaviours.BasicCharger do
  @moduledoc """
    Behaviour of a Basic Charger, this is a charger behaviour with a limit set of actions
  """

  alias OcppModel.V16.Messages, as: M

  @callback reset(M.ResetRequest.t(), any()) ::
              {{:ok, M.ResetResponse.t()}, any()} | {{:error, :reset, String.t()}, any()}

  # @callback unlock_connector(M.UnlockConnectorRequest.t(), any()) ::
  #             {{:ok, M.UnlockConnectorResponse.t()}, any()}
  #             | {{:error, :unlock_connector, String.t()}, any()}

  defp model_and_method(action, impl) do
    %{
      "Reset" => {M.ResetRequest, &impl.reset/2},
      # "UnlockConnector" => {M.UnlockConnectorRequest, &impl.unlock_connector/2}
    } |> Map.get(action)
  end

  @spec handle(atom, String.t(), map, any) :: {{:ok, map}, any} | {{:error, atom, binary}, any}
  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions
  """
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
