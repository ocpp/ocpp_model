defmodule Generators.V20.DataTypes do
  @moduledoc false

  use ExUnitProperties

  alias OcppModel.V20.DataTypes, as: DT

  import Generators.V20.Generic

  def ac_charging_parameters_generator do
    gen all(
          gen_amount <- integer_generator(),
          gen_min_current <- integer_generator(),
          gen_max_current <- integer_generator(),
          gen_max_voltage <- integer_generator()
        ) do
      %DT.ACChargingParameters{
        energyAmount: gen_amount,
        evMinCurrent: gen_min_current,
        evMaxCurrent: gen_max_current,
        evMaxVoltage: gen_max_voltage
      }
    end
  end

  def additional_info_generator do
    gen all(
          gen_add_id_token <- string_generator(36),
          gen_type <- string_generator(50)
        ) do
      %DT.AdditionalInfo{
        additionalIdToken: gen_add_id_token,
        type: gen_type
      }
    end
  end

  def authorization_data_generator do
    gen all(
          gen_id_token <- id_token_generator(),
          gen_id_token_info <- id_token_info_generator()
        ) do
      %DT.AuthorizationData{
        idToken: gen_id_token,
        idTokenInfo: gen_id_token_info
      }
    end
  end

  def cert_hash_generator do
    gen all(
          gen_hash_algo <- enum_generator(:hashAlgorithm),
          gen_issuer_name_hash <- string_generator(512),
          gen_issuer_key_hash <- string_generator(128),
          gen_serial <- string_generator(40)
        ) do
      %DT.CertificateHashData{
        hashAlgorithm: gen_hash_algo,
        issuerNameHash: gen_issuer_name_hash,
        issureKeyHash: gen_issuer_key_hash,
        serialNumber: gen_serial
      }
    end
  end

  def cert_hash_data_chain_generator do
    gen all(
          gen_cert_type <- enum_generator(:getCertificateIdUse),
          gen_cert_hash_data <- cert_hash_generator(),
          gen_child_cert_hash_data <- list_of(cert_hash_generator(), length: 4)
        ) do
      %DT.CertificateHashDataChain{
        certificateType: gen_cert_type,
        certificateHashData: gen_cert_hash_data,
        childCertificateHashData: gen_child_cert_hash_data
      }
    end
  end

  def charging_limit_generator do
    gen all(
          gen_limit_src <- enum_generator(:chargingLimitSource),
          gen_grid_crit <- boolean_generator()
        ) do
      %DT.ChargingLimit{
        chargingLimitSource: gen_limit_src,
        isGridCritical: gen_grid_crit
      }
    end
  end

  def charging_needs_generator do
    gen all(
          gen_req_transfer <- enum_generator(:energyTransferMode),
          gen_departure <- date_time_generator(),
          gen_ac_params <- ac_charging_parameters_generator(),
          gen_dc_params <- dc_charging_parameters_generator()
        ) do
      %DT.ChargingNeeds{
        requestedEnergyTransfer: gen_req_transfer,
        departureTime: gen_departure,
        acChargingParameters: gen_ac_params,
        dcChargingParameters: gen_dc_params
      }
    end
  end

  def charging_schedule_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_start_schedule <- date_time_generator(),
          gen_duration <- integer_generator(),
          gen_rate_unit <- enum_generator(:chargingRateUnit),
          gen_min_rate <- float_generator(),
          gen_schedule_period <- charging_schedule_period_generator(),
          gen_tariff <- sales_tariff_generator()
        ) do
      %DT.ChargingSchedule{
        id: gen_id,
        startSchedule: gen_start_schedule,
        duration: gen_duration,
        chargingRateUnit: gen_rate_unit,
        minChargingRate: gen_min_rate,
        chargingSchedulePeriod: gen_schedule_period,
        salesTariff: gen_tariff
      }
    end
  end

  def charging_station_generator do
    gen all(
          gen_serial <- string_generator(25),
          gen_model <- string_generator(20),
          gen_vendor <- string_generator(50),
          gen_firmware <- string_generator(50),
          gen_modem <- modem_generator()
        ) do
      %DT.ChargingStation{
        serialNumber: gen_serial,
        model: gen_model,
        vendorName: gen_vendor,
        firmwareVersion: gen_firmware,
        modem: gen_modem
      }
    end
  end

  def charging_profile_criterion_generator do
    gen all(
          gen_purpose <- enum_generator(:chargingProfilePurpose),
          gen_stack_level <- integer_generator(),
          gen_profile_id <- list_of(integer(), length: 4),
          gen_limit_source <- list_of(enum_generator(:chargingLimitSource), length: 4)
        ) do
      %DT.ChargingProfileCriterion{
        chargingProfilePurpose: gen_purpose,
        stackLevel: gen_stack_level,
        chargingProfileId: gen_profile_id,
        chargingLimitSource: gen_limit_source
      }
    end
  end

  def charging_profile_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_stack_level <- integer_generator(),
          gen_purpose <- enum_generator(:chargingProfilePurpose),
          gen_kind <- enum_generator(:chargingProfileKind),
          gen_recurrency <- enum_generator(:recurrencyKind)
        ) do
      %DT.ChargingProfile{
        id: gen_id,
        stackLevel: gen_stack_level,
        chargingProfilePurpose: gen_purpose,
        chargingProfileKind: gen_kind,
        recurrencyKind: gen_recurrency
      }
    end
  end

  def charging_schedule_period_generator do
    gen all(
          gen_start_period <- integer_generator(),
          gen_limit <- float_generator(),
          gen_nr_phases <- integer_generator(1..3),
          gen_phase_to_use <- integer_generator(1..3)
        ) do
      %DT.ChargingSchedulePeriod{
        startPeriod: gen_start_period,
        limit: gen_limit,
        numberPhases: gen_nr_phases,
        phaseToUse: gen_phase_to_use
      }
    end
  end

  def clear_charging_profile_generator do
    gen all(
          gen_evse_id <- integer_generator(),
          gen_purpose <- enum_generator(:chargingProfilePurpose),
          gen_stack_level <- integer_generator()
        ) do
      %DT.ClearChargingProfile{
        evseId: gen_evse_id,
        chargingProfilePurpose: gen_purpose,
        stackLevel: gen_stack_level
      }
    end
  end

  def clear_monitoring_result_generator do
    gen all(
          gen_status <- enum_generator(:clearMonitoringStatus),
          gen_id <- integer_generator(),
          gen_status_info <- status_info_generator()
        ) do
      %DT.ClearMonitoringResult{
        status: gen_status,
        id: gen_id,
        statusInfo: gen_status_info
      }
    end
  end

  def composite_schedule_generator do
    gen all(
          gen_evse_id <- integer_generator(),
          gen_duration <- integer_generator(),
          gen_schedule_start <- date_time_generator(),
          gen_charging_rate_unit <- enum_generator(:chargingRateUnit),
          gen_charging_schedule <- charging_schedule_period_generator()
        ) do
      %DT.CompositeSchedule{
        evseId: gen_evse_id,
        duration: gen_duration,
        scheduleStart: gen_schedule_start,
        chargingRateUnit: gen_charging_rate_unit,
        chargingSchedulePeriod: gen_charging_schedule
      }
    end
  end

  def component_generator do
    gen all(
          gen_name <- string_generator(50),
          gen_instance <- string_generator(50),
          gen_evse <- evse_generator()
        ) do
      %DT.Component{
        name: gen_name,
        instance: gen_instance,
        evse: gen_evse
      }
    end
  end

  def component_variable_generator do
    gen all(
          gen_component <- component_generator(),
          gen_var <- variable_generator()
        ) do
      %DT.ComponentVariable{
        component: gen_component,
        variable: gen_var
      }
    end
  end

  def consumption_cost_generator do
    gen all(
          gen_start_value <- float_generator(),
          gen_cost <- list_of(cost_generator())
        ) do
      %DT.ConsumptionCost{
        startValue: gen_start_value,
        cost: gen_cost
      }
    end
  end

  def cost_generator do
    gen all(
          gen_kind <- enum_generator(:costKind),
          gen_amount <- integer_generator(),
          gen_multipler <- integer_generator(-3..3)
        ) do
      %DT.Cost{
        costKind: gen_kind,
        amount: gen_amount,
        amountMultiplier: gen_multipler
      }
    end
  end

  def dc_charging_parameters_generator do
    gen all(
          gen_max_current <- integer_generator(),
          gen_max_voltage <- integer_generator(),
          gen_amount <- integer_generator(),
          gen_max_power <- integer_generator(),
          gen_soc <- integer_generator(0..100)
        ) do
      %DT.DCChargingParameters{
        evMaxCurrent: gen_max_current,
        evMaxVoltage: gen_max_voltage,
        energyAmount: gen_amount,
        evMaxPower: gen_max_power,
        stateOfCharge: gen_soc
      }
    end
  end

  def event_data_generator do
    gen all(
          gen_event_id <- integer_generator(),
          gen_timestamp <- date_time_generator(),
          gen_trigger <- enum_generator(:eventTrigger),
          gen_cause <- integer_generator(),
          gen_value <- string_generator(2500),
          gen_tech_code <- string_generator(50),
          gen_tech_info <- string_generator(50),
          gen_cleared <- boolean_generator(),
          gen_transaction_id <- string_generator(36),
          gen_var_mon_id <- integer_generator(),
          gen_event_notification <- enum_generator(:eventNotification),
          gen_comp <- component_generator(),
          gen_var <- variable_generator()
        ) do
      %DT.EventData{
        eventId: gen_event_id,
        timestamp: gen_timestamp,
        trigger: gen_trigger,
        cause: gen_cause,
        actualValue: gen_value,
        techCode: gen_tech_code,
        techInfo: gen_tech_info,
        cleared: gen_cleared,
        transactionId: gen_transaction_id,
        variableMonitoringId: gen_var_mon_id,
        eventNotificationType: gen_event_notification,
        component: gen_comp,
        variable: gen_var
      }
    end
  end

  def evse_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_conn_id <- integer_generator()
        ) do
      %DT.Evse{
        id: gen_id,
        connector_id: gen_conn_id
      }
    end
  end

  def id_token_generator do
    gen all(
          gen_id_token <- string_generator(36),
          gen_itet <- enum_generator(:idToken),
          gen_additional_info_generator <- list_of(additional_info_generator(), length: 4)
        ) do
      %DT.IdToken{
        idToken: gen_id_token,
        type: gen_itet,
        additionalInfo: gen_additional_info_generator
      }
    end
  end

  def id_token_info_generator do
    gen all(
          gen_status <- enum_generator(:authorizationStatus),
          gen_cache_expiry <- date_time_generator(),
          gen_charging_prio <- integer_generator(),
          gen_lan1 <- string_generator(8),
          gen_evse_id <- list_of(integer_generator(), length: 4),
          gen_lan2 <- string_generator(8),
          gen_group_id <- id_token_generator(),
          gen_personal_message <- personal_message_generator()
        ) do
      %DT.IdTokenInfo{
        status: gen_status,
        cacheExpiryDateTime: gen_cache_expiry,
        chargingPriority: gen_charging_prio,
        language1: gen_lan1,
        evseId: gen_evse_id,
        language2: gen_lan2,
        groupIdToken: gen_group_id,
        personalMessage: gen_personal_message
      }
    end
  end

  def get_variable_data_generator do
    gen all(
          gen_attr_type <- enum_generator(:attribute),
          gen_comp <- component_generator(),
          gen_var <- variable_generator()
        ) do
      %DT.GetVariableData{
        attributeType: gen_attr_type,
        component: gen_comp,
        variable: gen_var
      }
    end
  end

  def get_variable_result_generator do
    gen all(
          gen_attr_status <- enum_generator(:getVariableStatus),
          gen_attr_type <- enum_generator(:attribute),
          gen_attr_val <- string_generator(2500),
          gen_comp <- component_generator()
        ) do
      %DT.GetVariableResult{
        attributeStatus: gen_attr_status,
        attributeType: gen_attr_type,
        attributeValue: gen_attr_val,
        component: gen_comp
      }
    end
  end

  def log_parameters_generator do
    gen all(
          gen_location <- string_generator(512),
          gen_oldest <- date_time_generator(),
          gen_latest <- date_time_generator()
        ) do
      %DT.LogParameters{
        remoteLocation: gen_location,
        oldestTimestamp: gen_oldest,
        latestTimestamp: gen_latest
      }
    end
  end

  def modem_generator do
    gen all(
          gen_iccid <- string_generator(20),
          gen_imsi <- string_generator(20)
        ) do
      %DT.Modem{
        iccid: gen_iccid,
        imsi: gen_imsi
      }
    end
  end

  def message_content_generator do
    gen all(
          gen_format <- enum_generator(:messageFormat),
          gen_lan <- string_generator(8),
          gen_content <- string_generator(512)
        ) do
      %DT.MessageContent{
        format: gen_format,
        language: gen_lan,
        content: gen_content
      }
    end
  end

  def message_info_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_prio <- enum_generator(:messagePriority),
          gen_state <- enum_generator(:messageState),
          gen_start <- date_time_generator(),
          gen_end <- date_time_generator(),
          gen_transaction_id <- string_generator(36),
          gen_message <- message_content_generator()
        ) do
      %DT.MessageInfo{
        id: gen_id,
        priority: gen_prio,
        state: gen_state,
        startDateTime: gen_start,
        endDateTime: gen_end,
        transactionId: gen_transaction_id,
        message: gen_message
      }
    end
  end

  def meter_value_type_generator do
    gen all(
          gen_datetime <- date_time_generator(),
          gen_value <- float_generator(),
          gen_context <- enum_generator(:readingContext),
          gen_measurand <- enum_generator(:measurand),
          gen_phase <- enum_generator(:phase),
          gen_location <- enum_generator(:location),
          gen_signedmeterdata <- string_generator(2500),
          gen_signingmethod <- string_generator(50),
          gen_encodingmethod <- string_generator(50),
          gen_publickey <- string_generator(2500),
          gen_unit <- string_generator(20),
          gen_multiplier <- integer_generator()
        ) do
      %DT.MeterValue{
        timestamp: gen_datetime,
        sampledValue: %DT.SampledValue{
          value: gen_value,
          context: gen_context,
          measurand: gen_measurand,
          phase: gen_phase,
          location: gen_location,
          signedMeterValue: %DT.SignedMeterValue{
            signedMeterData: gen_signedmeterdata,
            signingMethod: gen_signingmethod,
            encodingMethod: gen_encodingmethod,
            publicKey: gen_publickey
          },
          unitOfMeasure: %DT.UnitOfMeasure{
            unit: gen_unit,
            multiplier: gen_multiplier
          }
        }
      }
    end
  end

  def monitoring_data_generator do
    gen all(
          gen_comp <- component_generator(),
          gen_var <- variable_generator(),
          gen_var_mon <- variable_monitoring_generator()
        ) do
      %DT.MonitoringData{
        component: gen_comp,
        variable: gen_var,
        variableMonitoring: gen_var_mon
      }
    end
  end

  def ocsp_req_data_generator do
    gen all(
          hash_algo <- enum_generator(:hashAlgorithm),
          issuer_name_hash <- string_generator(512),
          issuer_key_hash <- string_generator(128),
          serial <- string_generator(40),
          responder_url <- string_generator(512)
        ) do
      %DT.OCSPRequestData{
        hashAlgorithm: hash_algo,
        issuerNameHash: issuer_name_hash,
        issuerKeyHash: issuer_key_hash,
        serialNumber: serial,
        responderURL: responder_url
      }
    end
  end

  def personal_message_generator do
    gen all(
          gen_format <- enum_generator(:messageFormat),
          gen_language <- string_generator(8),
          gen_content <- string_generator(512)
        ) do
      %DT.MessageContent{
        format: gen_format,
        language: gen_language,
        content: gen_content
      }
    end
  end

  def relative_time_interval_generator do
    gen all(
          gen_start <- integer_generator(),
          gen_duration <- integer_generator()
        ) do
      %DT.RelativeTimeInterval{
        start: gen_start,
        duration: gen_duration
      }
    end
  end

  def report_data_generator do
    gen all(gen_comp <- component_generator()) do
      %DT.ReportData{
        component: gen_comp
      }
    end
  end

  def sales_tariff_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_desc <- string_generator(32),
          gen_levels <- integer_generator(),
          gen_entry <- sales_tariff_entry_generator()
        ) do
      %DT.SalesTariff{
        id: gen_id,
        salesTariffDescription: gen_desc,
        numEPriceLevels: gen_levels,
        salesTariffEntry: gen_entry
      }
    end
  end

  def sales_tariff_entry_generator do
    gen all(
          gen_eprice_level <- integer_generator(),
          gen_rel_time_int <- relative_time_interval_generator(),
          gen_consumption_cost <- consumption_cost_generator()
        ) do
      %DT.SalesTariffEntry{
        ePriceLevel: gen_eprice_level,
        relativeTimeInterval: gen_rel_time_int,
        consumptionCost: gen_consumption_cost
      }
    end
  end

  def status_info_generator do
    gen all(
          gen_reason_code <- string_generator(20),
          gen_additional_info_generator <- string_generator(512)
        ) do
      %DT.StatusInfo{
        reasonCode: gen_reason_code,
        additionalInfo: gen_additional_info_generator
      }
    end
  end

  def transaction_info_generator do
    gen all(
          gen_transaction_id <- string_generator(36),
          gen_charging_state <- enum_generator(:chargingState),
          gen_time_spent_charging <- integer_generator(),
          gen_stopped_reason <- enum_generator(:reason),
          gen_remote_start_id <- integer_generator()
        ) do
      %DT.Transaction{
        transactionId: gen_transaction_id,
        chargingState: gen_charging_state,
        timeSpentCharging: gen_time_spent_charging,
        stoppedReason: gen_stopped_reason,
        remoteStartId: gen_remote_start_id
      }
    end
  end

  def variable_generator do
    gen all(gen_name <- string_generator(50)) do
      %DT.Variable{
        name: gen_name
      }
    end
  end

  def variable_monitoring_generator do
    gen all(
          gen_id <- integer_generator(),
          gen_transaction <- boolean_generator(),
          gen_value <- float_generator(),
          gen_type <- enum_generator(:monitor),
          gen_severity <- integer_generator()
        ) do
      %DT.VariableMonitoring{
        id: gen_id,
        transaction: gen_transaction,
        value: gen_value,
        type: gen_type,
        severity: gen_severity
      }
    end
  end
end
