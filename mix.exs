defmodule OcppModel.MixProject do
  use Mix.Project

  @version "0.3.1"
  @elixir_version "~> 1.11.2"

  def project do
    [
      app: :ocpp_model,
      description: "Contains all you need to implement a OCPP Charger or ChargeSystem",
      version: @version,
      elixir: @elixir_version,
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      deps: deps(),
      aliases: aliases(),
      docs: docs(),
      package: package(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [coveralls: :test, "coveralls.json": :test, "coveralls.html": :test]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/generators", "test/implementations"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:typed_struct, "~> 0.2.1"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:credo, "~> 1.5.0-rc-2", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.10", only: :test},
      {:stream_data, "~> 0.5", only: :test}
    ]
  end

  defp aliases do
    [
      all: ["compile --warnings-as-errors", "coveralls --trace", "credo --strict"],
      tt: ["test --trace"],
      cov: ["coveralls", "coveralls.json"]
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: [
        "README.md",
        "LICENSE",
        "messages_2.0.1.md",
        "messages_1.6.md"
      ],
      source_ref: "v#{@version}",
      source_url: "https://github.com/gertjana/ocpp_model"
    ]
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README.md", "LICENSE*"],
      maintainers: ["Gertjan Assies"],
      licenses: ["Apache 2.0"],
      links: %{"GitLab" => "https://gitlab.com/gertjana/ocpp_model"}
    ]
  end
end
