# OCPP v2.0.1 Messages
 
List of OCPP 2.0.1 Messages, Data Types and Enumerations which are implemented in this library

 * [Messages](#messages)
 * [DataTypes](#datatypes)
 * [Enumerations](#enumerations)

## how to implement a message

 * Add Request and Response structs to `/lib/ocpp_model/v20/messages.ex`
 * Add any new DataTypes to `/lib/ocpp_model/v20/datatypes.ex`
 * Add any new EnumTypes to `/lib/ocpp_model/v20/enumtypes.ex`
 * Add property generators to the correct `/test/generators/*` modules and the property tests to `test/ocpp_model_messages_test.ex` 
 * Add a callback and handle clause to either the `/lib/ocpp_model/v20/behaviours/charger_behaviour.ex` or `/lib/ocpp_model/v20/behaviours/chargesystem_behaviour.ex`
 * Implement the behaviour in either the test Charger or ChargeSystem and write a test for it in either `test/ocpp_charger_test.ex` or `test/ocpp_chargesystem_test.ex`
 * run `MIX_ENV=test mix all` for any test, coverage or linting issues

## Messages
Ch = Chargestation, CS = ChargerSystem, CO = Controller


| Direction | Message                           | Implemented |
| --------- | --------------------------------- | ----------- |
| Ch -> CS  | Authorize                         | yes         |
| Ch -> CS  | BootNotification                  | yes         |
| CS -> Ch  | CancelReservation                 | yes         |
| CS -> Ch  | CertificateSigned                 | yes         |
| CS -> Ch  | ChangeAvailability                | yes         |
| CS -> Ch  | ClearCache                        | yes         |
| CS -> Ch  | ClearChargingProfile              | yes         |
| CS -> Ch  | ClearDisplayMessage               | yes         |
| Ch -> CS  | ClearedChargingLimit              | yes         |
| CS -> Ch  | ClearVariableMonitoring           | yes         |
| CS -> Ch  | CostUpdated                       | yes         |
| CS -> Ch  | CustomerInformation               | yes         |
| Ch <> CS  | DataTransfer                      | yes         |
| CS -> Ch  | DeleteCertificate                 | yes         |
| Ch -> CS  | FirmwareStatusNotification        | yes         |
| Ch -> CS  | Get15118EVCertificate             | yes         |
| CS -> Ch  | GetBaseReport                     | yes         |
| Ch -> CS  | GetCertificateStatus              | yes         |
| CS -> Ch  | GetChargingProfiles               | yes         |
| CS -> Ch  | GetCompositeSchedule              | yes         |
| CS -> Ch  | GetDisplayMessages                | yes         |
| CS -> Ch  | GetInstalledCertificateIds        | yes         |
| CS -> Ch  | GetLocalListVersion               | yes         |
| CS -> Ch  | GetLog                            | yes         |
| CS -> Ch  | GetMonitoringReport               | yes         |
| CS -> Ch  | GetReport                         | yes         |
| CS -> Ch  | GetTransactionStatus              | yes         |
| CS -> Ch  | GetVariables                      | yes         |
| Ch -> CS  | Heartbeat                         | yes         |
| CS -> Ch  | InstallCertificate                | yes         |
| Ch -> CS  | LogStatusNotification             | yes         |
| CS -> Ch  | MeterValues                       | yes         |
| Ch -> CS  | NotifyChargingLimit               | yes         |
| Ch -> CS  | NotifyCustomerInformation         | yes         |
| Ch -> CS  | NotifyDisplayMessages             | yes         |
| Ch -> CS  | NotifyEVChargingNeeds             | yes         |
| Ch -> CS  | NotifyEVChargingSchedule          | yes         |
| Ch -> CS  | NotifyEvent                       | yes         |
| Ch -> CS  | NotifyMonitoringReport            | yes         |
| Ch -> CS  | NotifyReport                      | yes         |
| CS -> CO  | PublishFirmware                   | yes         |
| Ch -> CS  | PublishFirmwareStatusNotification | yes         |
| Ch -> CS  | ReportChargingProfiles            | yes         |
| CS -> Ch  | RequestStartTransaction           | yes         |
| CS -> Ch  | RequestStopTransaction            | yes         |
| Ch -> CS  | ReservationStatusUpdate           | yes         |
| CS -> Ch  | ReserveNow                        | yes         |
| CS -> Ch  | Reset                             | yes         |
| Ch -> CS  | SecurityEventNotification         | yes         |
| CS -> Ch  | SendLocalList                     | yes         |
| CS -> Ch  | SetChargingProfile                | yes         |
| CS -> Ch  | SetDisplayMessage                 | yes         |
| CS -> Ch  | SetMonitoringBase                 | no          |
| CS -> Ch  | SetMonitoringLevel                | no          |
| CS -> Ch  | SetNetworkProfile                 | no          |
| CS -> Ch  | SetVariableMonitoring             | no          |
| CS -> Ch  | SetVariables                      | no          |
| Ch -> CS  | SignCertificate                   | no          |
| Ch -> CS  | StatusNotification                | yes         |
| Ch -> CS  | TransactionEvent                  | yes         |
| CS -> Ch  | TriggerMessage                    | yes         |
| CS -> Ch  | UnlockConnector                   | yes         |
| CS -> Ch  | UnpublishFirmware                 | no          |
| CS -> Ch  | UpdateFirmware                    | no          |

## DataTypes

| DataType                 | Implemented |
| ------------------------ | ----------- |
| ACChargingParameters     | yes         |
| AdditionalInfo           | yes         |
| APN                      | no          |
| AuthorizationData        | yes         |
| CertificateHashDataChain | yes         |
| CertificateHashData      | yes         |
| ChargingLimit            | yes         |
| ChargingNeeds            | yes         |
| ChargingProfileCriterion | yes         |
| ChargingProfile          | yes         |
| ChargingSchedulePeriod   | yes         |
| ChargingSchedule         | yes         |
| ChargingStation          | yes         |
| ClearChargingProfile     | yes         |
| ClearMonitoringResult    | yes         |
| Component                | yes         |
| ComponentVariable        | yes         |
| CompositeSchedule        | yes         |
| ConsumptionCost          | yes         |
| Cost                     | yes         |
| DCChargingParameters     | yes         |
| EventData                | yes         |
| EVSE                     | yes         |
| Firmware                 | no          |
| GetVariableData          | yes         |
| GetVariableResult        | yes         |
| IdTokenInfo              | yes         |
| IdToken                  | yes         |
| LogParameters            | yes         |
| MessageContent           | yes         |
| MessageInfo              | yes         |
| MeterValue               | yes         |
| Modem                    | yes         |
| MonitoringData           | yes         |
| NetworkConnectionProfile | no          |
| OCSPRequestData          | yes         |
| RelativeTimeInterval     | yes         |
| ReportData               | yes         |
| SalesTariffEntry         | yes         |
| SalesTariff              | yes         |
| SampledValue             | yes         |
| SetMonitoringData        | no          |
| SetMonitoringResult      | no          |
| SetVariableData          | no          |
| SetVariableResult        | no          |
| SignedMeterValue         | yes         |
| StatusInfo               | yes         |
| Transaction              | yes         |
| UnitOfMeasure            | yes         |
| VariableAttribute        | no          |
| VariableCharacteristics  | no          |
| VariableMonitoring       | yes         |
| Variable                 | yes         |
| VPN                      | no          |

## Enumerations

| Enumerations                  | Implemented |
| ----------------------------- | ----------- |
| APNAuthentication             | no          |
| Attribute                     | yes         |
| AuthorizationStatus           | yes         |
| AuthorizeCertificateStatus    | yes         |
| BootReason                    | yes         |
| CancelReservationStatus       | yes         |
| CertificateAction             | yes         |
| CertificateSignedStatus       | yes         |
| CertificateSigningUse         | yes         |
| ChangeAvailabilityStatus      | yes         |
| ChargingLimitSource           | yes         |
| ChargingProfileKind           | yes         |
| ChargingProfilePurpose        | yes         |
| ChargingProfileStatus         | yes         |
| ChargingRateUnit              | yes         |
| ChargingState                 | yes         |
| ClearCacheStatus              | yes         |
| ClearChargingProfileStatus    | yes         |
| ClearMessageStatus            | yes         |
| ClearMonitoringStatus         | yes         |
| ComponentCriterion            | yes         |
| Connector                     | yes         |
| ConnectorStatus               | yes         |
| CostKind                      | yes         |
| CustomerInformationStatus     | yes         |
| Data                          | no          |
| DataTransferStatus            | yes         |
| DeleteCertificateStatus       | yes         |
| DisplayMessageStatus          | yes         |
| EnergyTransferMode            | yes         |
| EventNotification             | yes         |
| EventTrigger                  | yes         |
| FirmwareStatus                | yes         |
| GenericDeviceModelStatus      | yes         |
| GenericStatus                 | yes         |
| GetCertificateIdUse           | yes         |
| GetCertificateStatus          | yes         |
| GetChargingProfileStatus      | yes         |
| GetDisplayMessagesStatus      | yes         |
| GetInstalledCertificateStatus | yes         |
| GetVariableStatus             | yes         |
| HashAlgorithm                 | yes         |
| IdToken                       | yes         |
| InstallCertificateStatus      | yes         |
| InstallCertificateUse         | yes         |
| Iso15118EVCertificateStatus   | yes         |
| Location                      | yes         |
| Log                           | yes         |
| LogStatus                     | yes         |
| Measurand                     | yes         |
| MessageFormat                 | yes         |
| MessagePriority               | yes         |
| MessageState                  | yes         |
| MessageTrigger                | yes         |
| Monitor                       | yes         |
| MonitoringBase                | no          |
| MonitoringCriterion           | yes         |
| Mutability                    | no          |
| NotifyEVChargingNeedsStatus   | yes         |
| OCPPInterface                 | no          |
| OCPPTransport                 | no          |
| OCPPVersion                   | no          |
| OperationalStatus             | yes         |
| Phase                         | yes         |
| PublishFirmwareStatus         | yes         |
| ReadingContext                | yes         |
| Reason                        | yes         |
| RecurrencyKind                | yes         |
| RegistrationStatus            | yes         |
| ReportBase                    | yes         |
| RequestStartStopStatus        | yes         |
| ReservationUpdateStatus       | yes         |
| ReserveNowStatus              | yes         |
| Reset                         | yes         |
| ResetStatus                   | yes         |
| SendLocalListStatus           | yes         |
| SetMonitoringStatus           | no          |
| SetNetworkProfileStatus       | no          |
| SetVariableStatus             | no          |
| TransactionEvent              | yes         |
| TriggerMessageStatus          | yes         |
| TriggerReason                 | yes         |
| UnlockStatus                  | yes         |
| UnpublishFirmwareStatus       | no          |
| Update                        | yes         |
| UpdateFirmwareStatus          | no          |
| UploadLogStatus               | yes         |
| VPN                           | no          |
