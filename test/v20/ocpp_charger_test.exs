defmodule OcppModel.V20.ChargerTest do
  use ExUnit.Case

  alias Implementations.V20.MyTestBasicCharger
  alias Implementations.V20.MyTestCharger

  @state %{}

  test "MyTestCharger.handle method should give a CallResult response when a correct CancelReservation Call message is given" do
    message = [2, "42", "CancelReservation", %{reservationId: 1}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct Certificate Call message is given" do
    message = [2, "42", "CertificateSigned", %{certificateChain: "a long string"}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a ChangeAvailability Call message is given" do
    message = [2, "42", "ChangeAvailability", %{operationalStatus: "Inoperative", evse: 0}]
    expected = [
      3,
      "42",
      %{status: "Accepted", statusInfo: %{reasonCode: "charger is inoperative"}}
    ]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct ClearCache Call message is given" do
    message = [2, "42", "ClearCache", %{}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct ClearChargingProfile Call message is given" do
    message = [2, "42", "ClearChargingProfile", %{}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct ClearDisplayMessage Call message is given" do
    message = [2, "42", "ClearDisplayMessage", %{id: 1}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct ClearVariableMonitoring Call message is given" do
    message = [2, "42", "ClearVariableMonitoring", %{id: [1]}]

    assert {[3, "42", %{clearMonitoringResult: [%{status: "Accepted", id: 1}]}], %{}} ==
             MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct CostUpdated Call message is given" do
    message = [2, "42", "CostUpdated", %{totalCost: 3.14, transactionId: "NC-00001-123"}]
    assert {[3, "42", %{}], %{}} = MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct CustomerInformation Call message is given" do
    message = [2, "42", "CustomerInformation", %{requestId: 1337, report: true, clear: false}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct DataTransfer Call message is given" do
    message = [
      2,
      "42",
      "DataTransfer",
      %{messageId: "001", data: "All your base are belong to us", vendorId: "GA"}
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a correct DeleteCertificate Call message is given" do
    message = [
      2,
      "42",
      "DeleteCertificate",
      %{
        certificateHashData: %{
          hashAlgorithm: "SHA512",
          issuerNameHash: "adsfksdofspdofipsodfi",
          issureKeyHash: "asdfjsldkfjsldkfjslkdfj",
          serialNumber: "001"
        }
      }
    ]

    assert {[3, "42", %{status: "Accepted", statusInfo: %{reasonCode: "cert deleted"}}], %{}} ==
             MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetBaseReport Call message is given" do
    message = [2, "42", "GetBaseReport", %{requestId: 1337, reportBase: "FullInventory"}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetChargingProfiles Call message is given" do
    message = [2, "42", "GetChargingProfiles", %{requestId: 1337, chargingProfile: %{}}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetComposite Call message is given" do
    message = [2, "42", "GetCompositeSchedule", %{duration: 3600, evseId: 0}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetDisplayMessages Call message is given" do
    message = [2, "42", "GetDisplayMessages", %{requestId: 73}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetInstalledCertifcateIds Call message is given" do
    message = [2, "42", "GetInstalledCertificateIds", %{}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetLocalListVersion Call message is given" do
    message = [2, "42", "GetLocalListVersion", %{}]
    assert {[3, "42", %{versionNumber: 73}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetLog Call message is given" do
    message = [
      2,
      "42",
      "GetLog",
      %{logType: "DiagnosticsLog", requestId: 73, log: %{remoteLocation: "some location"}}
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetMonitoringReport Call message is given" do
    message = [2, "42", "GetMonitoringReport", %{requestId: 73}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetReport Call message is given" do
    message = [2, "42", "GetReport", %{requestId: 73}]
    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetTransactionStatus Call message is given" do
    message = [2, "42", "GetTransactionStatus", %{transactionId: "NC-0001_1337"}]

    assert {[3, "42", %{ongoingIndicator: false, messagesInQueue: false}], %{}} ==
             MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a GetVariables Call message is given" do
    message = [
      2,
      "42",
      "GetVariables",
      %{getVariableData: [%{component: %{name: "Foo"}, variable: %{name: "Bar"}}]}
    ]

    assert {[
             3,
             "42",
             %{getVariableResult: [%{attributeStatus: "Accepted", component: %{name: "Foo"}}]}
           ], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a InstalledCertifcate Call message is given" do
    message = [
      2,
      "42",
      "InstallCertificate",
      %{certificateType: "V2GCertificate", certificate: "some base64 encoded certificate"}
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a RequestStartTransaction Call message is given" do
    message = [
      2,
      "42",
      "RequestStartTransaction",
      %{remoteStartId: 1337, idToken: %{idToken: "blabla", type: "Central"}}
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a RequestStopTransaction Call message is given" do
    message = [2, "42", "RequestStopTransaction", %{transactionId: 1337}]
    assert {[3, "42", %{}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a ReserveNow Call message is given" do
    message = [
      2,
      "42",
      "ReserveNow",
      %{
        id: 73,
        expiryDateTime: "20210101T01:00:00.000Z",
        idToken: %{idToken: "NL-B-002345-7", type: "Central"}
      }
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a Reset Call message is given" do
    message = [2, "42", "Reset", %{type: "OnIdle"}]
    expected = [3, "42", %{status: "Scheduled"}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
    assert {expected, %{}} == MyTestBasicCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a SendLocalList Call message is given" do
    message = [2, "42", "SendLocalList", %{versioNumber: 73, updateType: "Full"}]
    expected = [3, "42", %{status: "Accepted"}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a SetChargingProfile Call message is given" do
    message = [
      2,
      "42",
      "SetChargingProfile",
      %{
        evseId: 1,
        chargingProfile: %{
          id: 1,
          stackLevel: 1,
          chargingProfilePurpose: "TxDefaultProfile",
          chargingProfileKind: "Absolute"
        }
      }
    ]

    expected = [3, "42", %{status: "Accepted"}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a SetDisplayMessage Call message is given" do
    message = [
      2,
      "42",
      "SetDisplayMessage",
      %{
        message: %{
          id: 73,
          priority: "AlwaysFront",
          message: %{format: "UTF8", content: "All your base are belong to us"}
        }
      }
    ]

    expected = [3, "42", %{status: "Accepted"}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a UnlockConnector Call message is given" do
    message = [2, "42", "UnlockConnector", %{evseId: 0}]
    expected = [3, "42", %{status: "Unlocked", statusInfo: %{reasonCode: "cable unlocked"}}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
    assert {expected, %{}} == MyTestBasicCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallResult response when a TriggerMessage Call message is given" do
    message = [2, "42", "TriggerMessage", %{requestedMessage: "Heartbeat"}]
    expected = [3, "42", %{status: "NotImplemented"}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallError response when a incorrect Call message is given" do
    message = [2, "42", "Unknown", %{}]
    expected = [4, "42", "unknown_action", "The action Unknown is unknown", {}]
    assert {expected, %{}} == MyTestCharger.handle(message, @state)
    assert {expected, %{}} == MyTestBasicCharger.handle(message, @state)
  end
end
