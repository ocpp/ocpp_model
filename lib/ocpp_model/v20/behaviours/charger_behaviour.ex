defmodule OcppModel.V20.Behaviours.Charger do
  @moduledoc """
    Behaviour of a Charger, allowing the module assuming the behaviour to be able to respond to messages send to it
  """

  alias OcppModel.V20.Messages, as: M

  @callback cancel_reservation(M.CancelReservationRequest.t(), any()) ::
              {{:ok, M.CancelReservationResponse.t()}, any}
              | {{:error, :cancel_reservation, String.t()}, any}

  @callback certificate_signed(M.CertificateSignedRequest.t(), any) ::
              {{:ok, M.CertificateSignedResponse.t()}, any}
              | {{:error, :certificate_signed, String.t()}, any}

  @callback change_availability(M.ChangeAvailabilityRequest.t(), any) ::
              {{:ok, M.ChangeAvailabilityResponse.t()}, any}
              | {{:error, :change_availability, String.t()}, any}

  @callback clear_cache(M.ClearCacheRequest.t(), any) ::
              {{:ok, M.ClearCacheResponse.t()}, any} | {{:error, :clear_cache, String.t()}, any}

  @callback clear_charging_profile(M.ClearChargingProfileRequest.t(), any) ::
              {{:ok, M.ClearChargingProfileResponse.t()}, any}
              | {{:error, :clear_charging_profile, String.t()}, any}

  @callback clear_display_message(M.ClearDisplayMessageRequest.t(), any) ::
              {{:ok, M.ClearDisplayMessageResponse.t()}, any}
              | {{:error, :clear_display_message, String.t()}, any}

  @callback clear_variable_monitoring(M.ClearVariableMonitoringRequest.t(), any) ::
              {{:ok, M.ClearVariableMonitoringResponse.t()}, any}
              | {{:error, :clear_variable_monitoring, String.t()}, any}

  @callback cost_updated(M.CostUpdatedRequest.t(), any) ::
              {{:ok, M.CostUpdatedResponse.t()}, any} | {{:error, :cost_updated, String.t()}, any}

  @callback customer_information(M.CustomerInformationRequest.t(), any) ::
              {{:ok, M.CustomerInformationResponse.t()}, any}
              | {{:error, :customer_information, String.t()}, any}

  @callback data_transfer(M.DataTransferRequest.t(), any) ::
              {{:ok, M.DataTransferResponse.t()}, any}
              | {{:error, :data_transfer, String.t()}, any}

  @callback delete_certificate(M.DeleteCertificateRequest.t(), any) ::
              {{:ok, M.DeleteCertificateResponse.t()}, any}
              | {{:error, :delete_certificate, String.t()}, any}

  @callback get_base_report(M.GetBaseReportRequest.t(), any) ::
              {{:ok, M.GetBaseReportResponse.t()}, any}
              | {{:error, :get_base_report, String.t()}, any}

  @callback get_charging_profiles(M.GetChargingProfilesRequest.t(), any) ::
              {{:ok, M.GetChargingProfilesResponse.t()}, any}
              | {{:error, :get_charging_profiles, String.t()}, any}

  @callback get_composite_schedule(M.GetCompositeScheduleRequest.t(), any) ::
              {{:ok, M.GetCompositeScheduleResponse.t()}, any}
              | {{:error, :get_composite_schedule, String.t()}, any}

  @callback get_display_messages(M.GetDisplayMessagesRequest.t(), any) ::
              {{:ok, M.GetDisplayMessagesResponse.t()}, any}
              | {{:error, :get_display_messages, String.t()}, any}

  @callback get_installed_certificate_ids(M.GetInstalledCertificateIdsRequest.t(), any) ::
              {{:ok, M.GetInstalledCertificateIdsResponse.t()}, any}

  @callback get_local_list_version(M.GetLocalListVersionRequest.t(), any) ::
              {{:ok, M.GetLocalListVersionResponse.t()}, any}
              | {{:error, :get_local_list_version, String.t()}, any}

  @callback get_log(M.GetLogRequest.t(), any) ::
              {{:ok, M.GetLogResponse.t()}, any} | {{:error, :get_log, String.t()}, any}

  @callback get_monitoring_report(M.GetMonitoringReportRequest.t(), any) ::
              {{:ok, M.GetMonitoringReportResponse.t()}, any}
              | {{:error, :get_monitoring_report, String.t()}, any}

  @callback get_report(M.GetReportRequest.t(), any) ::
              {{:ok, M.GetReportResponse.t()}, any} | {{:error, :get_report, String.t()}, any}

  @callback get_transaction_status(M.GetTransactionStatusRequest.t(), any) ::
              {{:ok, M.GetTransactionStatusResponse.t()}, any}
              | {{:error, :get_transaction_status, String.t()}, any}

  @callback get_variables(M.GetVariablesRequest.t(), any) ::
              {{:ok, M.GetVariablesResponse.t()}, any}
              | {{:error, :get_variables, String.t()}, any}

  @callback install_certificate(M.InstallCertificateRequest.t(), any) ::
              {{:ok, M.InstallCertificateResponse.t()}, any}
              | {{:error, :install_certificate, String.t()}, any}

  @callback request_start_transaction(M.RequestStartTransactionRequest.t(), any) ::
              {{:ok, M.RequestStartTransactionResponse.t()}, any}
              | {{:error, :request_start_transaction, String.t()}, any}

  @callback request_stop_transaction(M.RequestStopTransactionRequest.t(), any) ::
              {{:ok, M.RequestStopTransactionResponse.t()}, any}
              | {{:error, :request_stop_transaction, String.t()}, any}

  @callback reserve_now(M.ReserveNowRequest.t(), any) ::
              {{:ok, M.ReserveNowResponse.t()}, any} | {{:error, :reserve_now, String.t()}, any}

  @callback reset(M.ResetRequest.t(), any) ::
              {{:ok, M.ResetResponse.t()}, any} | {{:error, :reset, String.t()}, any}

  @callback send_local_list(M.SendLocalListRequest.t(), any) ::
              {{:ok, M.SendLocalListResponse.t()}, any}
              | {{:error, :send_local_list, String.t()}, any}

  @callback set_charging_profile(M.SetChargingProfileRequest.t(), any) ::
              {{:ok, M.SetChargingProfileResponse.t()}, any}
              | {{:error, :set_charging_profile, String.t()}, any}

  @callback set_display_message(M.SetDisplayMessageRequest.t(), any) ::
              {{:ok, M.SetDisplayMessageResponse.t()}, any}
              | {{:error, :set_display_message, String.t()}, any}

  @callback trigger_message(M.TriggerMessageRequest.t(), any) ::
              {{:ok, M.TriggerMessageResponse.t()}, any}
              | {{:error, :trigger_message, String.t()}, any}

  @callback unlock_connector(M.UnlockConnectorRequest.t(), any) ::
              {{:ok, M.UnlockConnectorResponse.t()}, any}
              | {{:error, :unlock_connector, String.t()}, any}

  defp model_and_method(action, impl) do
    %{
      "CancelReservation" => {M.CancelReservationRequest, &impl.cancel_reservation/2},
      "CertificateSigned" => {M.CertificateSignedRequest, &impl.certificate_signed/2},
      "ChangeAvailability" => {M.ChangeAvailabilityRequest, &impl.change_availability/2},
      "ClearCache" => {M.ClearCacheRequest, &impl.clear_cache/2},
      "ClearChargingProfile" => {M.ClearChargingProfileRequest, &impl.clear_charging_profile/2},
      "ClearDisplayMessage" => {M.ClearDisplayMessageRequest, &impl.clear_display_message/2},
      "ClearVariableMonitoring" =>
        {M.ClearVariableMonitoringRequest, &impl.clear_variable_monitoring/2},
      "CostUpdated" => {M.CostUpdatedRequest, &impl.cost_updated/2},
      "CustomerInformation" => {M.CostUpdatedRequest, &impl.customer_information/2},
      "DeleteCertificate" => {M.DeleteCertificateRequest, &impl.delete_certificate/2},
      "DataTransfer" => {M.DataTransferRequest, &impl.data_transfer/2},
      "GetBaseReport" => {M.GetBaseReportRequest, &impl.get_base_report/2},
      "GetChargingProfiles" => {M.GetChargingProfilesRequest, &impl.get_charging_profiles/2},
      "GetCompositeSchedule" => {M.GetCompositeScheduleRequest, &impl.get_composite_schedule/2},
      "GetDisplayMessages" => {M.GetDisplayMessagesRequest, &impl.get_display_messages/2},
      "GetInstalledCertificateIds" =>
        {M.GetDisplayMessagesRequest, &impl.get_installed_certificate_ids/2},
      "GetLocalListVersion" => {M.GetLocalListVersionRequest, &impl.get_local_list_version/2},
      "GetLog" => {M.GetLogRequest, &impl.get_log/2},
      "GetMonitoringReport" => {M.GetMonitoringReportRequest, &impl.get_monitoring_report/2},
      "GetReport" => {M.GetReportRequest, &impl.get_report/2},
      "GetTransactionStatus" => {M.GetTransactionStatusRequest, &impl.get_transaction_status/2},
      "GetVariables" => {M.GetVariablesRequest, &impl.get_variables/2},
      "InstallCertificate" => {M.InstallCertificateRequest, &impl.install_certificate/2},
      "RequestStartTransaction" =>
        {M.RequestStartTransactionRequest, &impl.request_start_transaction/2},
      "RequestStopTransaction" =>
        {M.RequestStopTransactionRequest, &impl.request_stop_transaction/2},
      "ReserveNow" => {M.ReserveNowRequest, &impl.reserve_now/2},
      "Reset" => {M.ResetRequest, &impl.reset/2},
      "SendLocalList" => {M.SendLocalListRequest, &impl.send_local_list/2},
      "SetChargingProfile" => {M.SetChargingProfileRequest, &impl.set_charging_profile/2},
      "SetDisplayMessage" => {M.SetDisplayMessageRequest, &impl.set_display_message/2},
      "TriggerMessage" => {M.TriggerMessageRequest, &impl.trigger_message/2},
      "UnlockConnector" => {M.UnlockConnectorRequest, &impl.unlock_connector/2}
    } |> Map.get(action)
  end

  @spec handle(atom(), String.t(), map(), any()) ::
          {{:ok, map()}, any} | {{:error, atom(), String.t()}, any}
  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions
  """
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
