defmodule OcppModel.V16.Messages do
  @moduledoc """
    OCPP 1.6 message structs
  """
  alias OcppModel.V16.DataTypes, as: DT

  defmodule AuthorizeRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :idToken, String.t(), enforce: true
    end
  end

  defmodule AuthorizeResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :idTagInfo, DT.IdTagInfo.t(), enforce: true
    end
  end

  defmodule BootNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :chargeBoxSerialNumber, String.t()
      field :chargePointModel, String.t(), enforce: true
      field :chargePointSerialNumber, String.t()
      field :chargePointVendor, String.t(), enforce: true
      field :firmwareVersion, String.t()
      field :iccid, String.t()
      field :imsi, String.t()
      field :meterSerialNumber, String.t()
      field :meterType, String.t()
    end
  end

  defmodule BootNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :currentTime, String.t(), enforce: true
      field :interval, integer(), enforce: true
      field :status, String.t(), enforce: true #RegistrationStatus
    end
  end

  defmodule HeartbeatRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do

    end
  end

  defmodule HeartbeatResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :currentTime, String.t(), enforce: true
    end
  end

  defmodule ResetRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :type, String.t(), enforce: true
    end
  end

  defmodule ResetResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :status, String.t(), enforce: true
    end
  end

  defmodule StartTransactionRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :connectorId, integer(), enforce: true
      field :idTag, String.t(), enforce: true
      field :meterStart, integer(), enforce: true
      field :reservactionId, integer()
      field :timestamp, String.t()
    end
  end

  defmodule StatusNotificationRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :connectorId, integer(), enforce: true
      field :errorCode, String.t(), enforce: true # ChargepointErrorCode
      field :info, String.t()
      field :status, String.t(), enforce: true # ChargepointStatus
      field :timestamp, String.t()
      field :vendorId, String.t()
      field :vendorErrorCode, String.t()
    end
  end

  defmodule StatusNotificationResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
    end
  end

  defmodule StartTransactionResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :idTagInfo, DT.IdTagInfo.t(), enforce: true
      field :transactionId, integer(), enforce: true
    end
  end

  defmodule StopTransactionRequest do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :idTag, String.t()
      field :meterStop, integer(), enforce: true
      field :timestamp, String.t(), enforce: true
      field :transactionId, integer(), enforce: true
      field :reason, String.t() # Reason
      field :transactionData, list(DT.Metervalue.t())
    end
  end

  defmodule StopTransactionResponse do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :idTagInfo, DT.IdTagInfo.t()
    end
  end
end
