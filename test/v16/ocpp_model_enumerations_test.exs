defmodule OcppModel.V16.EnumerationsTest do
  use ExUnit.Case

  alias OcppModel.V16.EnumTypes, as: ET

  test "valid enumeration" do
    assert ET.validate?(:resetType, "Soft")
  end

  test "wrong key enumeration" do
    assert !ET.validate?(:unknown, "Accepted")
  end

  test "wrong value enumeration" do
    assert !ET.validate?(:resetStatus, "Something")
  end

  test "get all enum types" do
    assert %{} = ET.get()
  end

  test "get a single enum type" do
    assert ["Accepted", "Rejected"] == ET.get(:resetStatus)
  end

  test "get an unknown enum type" do
    assert nil == ET.get(:foo)
  end
end
